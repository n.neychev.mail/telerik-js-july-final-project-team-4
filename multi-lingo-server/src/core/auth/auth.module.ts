import { AuthController } from './auth.contoller';
import { AuthService } from './auth.service';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JWTStrategy } from './Strategy/jwt.strategy';
import { UserModule } from '../../modules/user/user.module';

@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: 'jwt',
        }),
        JwtModule.register({
            secret: 'telerikisawesome',
            signOptions: { expiresIn: 24000 },
        }),
        UserModule,
    ],
    controllers: [AuthController],
    providers: [AuthService, JWTStrategy],
})
export class AuthModule { }
