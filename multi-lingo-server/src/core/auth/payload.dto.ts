import { Expose, Transform } from 'class-transformer';

export class PayloadDTO {
    @Expose()
    public id: string;

    @Expose()
    public username: string;
}
