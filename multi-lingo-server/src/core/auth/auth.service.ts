import { NotFoundException, UnauthorizedException, Injectable, Body } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { PayloadDTO } from './payload.dto';
import { TokenDTO } from './token.dto';
import { User } from 'src/database/entities/user.entity';
import { LogInUserDTO } from 'src/common/dto/user/login-user-dto';
import { UserService } from '../../modules/user/user.service';
@Injectable()
export class AuthService {
    private readonly blacklist: string[] = [];

    public constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
    ) { }

    public async login(body: LogInUserDTO): Promise<TokenDTO> {
        const foundUser: User = await this.userService.findUserByUsername(body.username);
        if (!foundUser) {
            throw new NotFoundException('Name does not match');
        }
        if (!await bcrypt.compare(body.password, foundUser.password)) {
            throw new UnauthorizedException('Password does not match');
        }
        const payload: PayloadDTO = {
            id: foundUser.id,
            username: foundUser.username,
        };
        return {
            token: await this.jwtService.signAsync(payload),
        };
    }

    public blacklistToken(token: string): void {
        this.blacklist.push(token);
    }

    public isTokenBlacklisted(token: string): boolean {
        return this.blacklist.includes(token);
    }
}
