import { Injectable, NotFoundException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { PayloadDTO } from '../payload.dto';
import { User } from 'src/database/entities/user.entity';
import { UserService } from '../../../modules/user/user.service';

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly userService: UserService
        ,
    ) {
        super( {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: 'telerikisawesome',
            ignoreExpiration: false,
        });
    }

    public async validate(payload: PayloadDTO): Promise<PayloadDTO> {
        const user: User = await this.userService.findUserByUsername(payload.username);

        if (!user) {
            throw new NotFoundException('No such user exists');
        }
        return {
            id: user.id,
            username: user.username,
        };
    }
}
