import { Controller, Post, Body, Delete, UseGuards } from '@nestjs/common';
import { TokenDTO } from './token.dto';
import { ApiUseTags } from '@nestjs/swagger';
import { LogInUserDTO } from '../../common/dto/user/login-user-dto';
import { AuthService } from './auth.service';
import { AuthGuardWithBlacklisting } from '../../common/guard/blacklist.guard';
import { Token } from '../../common/decorator/token.decorator';

@Controller('session')
@ApiUseTags('Authentication Controller')
export class AuthController {

    public constructor(
        private readonly authService: AuthService,
    ) { }

    @Post()
    public async login(
        @Body() body: LogInUserDTO,
    ): Promise<TokenDTO> {
        const jwttoken: TokenDTO = await this.authService.login(body);
        return jwttoken;
    }

    @Delete()
    @UseGuards(AuthGuardWithBlacklisting)
    public async logout(@Token() token: string) {
        this.authService.blacklistToken(token);

        return {
            msg: 'Successful logout!',
        };
    }
}
