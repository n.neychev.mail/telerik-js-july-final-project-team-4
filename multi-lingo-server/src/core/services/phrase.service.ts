
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Phrase } from '../../database/entities/phrase.entity';
import { Language } from '../../database/entities/language.entity';
import { Inject } from '@nestjs/common';
import { TranslationService } from '../../modules/translation/translation.service';

export class PhraseService {
    constructor(
        @InjectRepository(Phrase) private readonly phraseRepo: Repository<Phrase>,
        @InjectRepository(Language) private readonly languageRepo: Repository<Language>,
        @Inject(TranslationService) private readonly translationService: TranslationService,
    ) { }
    public async checkOrCreate(phrase: string) {
        let foundPhrase = await this.phraseRepo.findOne({
            where: {
                text: phrase,
            },
        });
        if (!foundPhrase) {
            foundPhrase = await this.createPhraseWithTranslations(phrase);
        }
        return foundPhrase;
    }

    private async createPhraseWithTranslations(phrase: string): Promise<Phrase> {
        const newPhrase = {
            text: phrase,
        };
        const createPhrase = this.phraseRepo.create(newPhrase);
        const savedPhrase = await this.phraseRepo.save(createPhrase);

        const languageArr = await this.languageRepo.find();
        languageArr.forEach((el: Language) => {
            this.translationService.createTranslation(savedPhrase.id, el.id);
        });
        return savedPhrase;
    }
}
