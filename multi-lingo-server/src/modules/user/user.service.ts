import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Injectable} from '@nestjs/common';
import { TranslationSystemError } from '../../common/exceptions/multilingo-system-error';
import { User } from '../../database/entities/user.entity';
import { Group } from '../../database/entities/group.entity';
import { GroupName } from '../../common/enum/group-name.enum';
import { Permission } from '../../database/entities/permission.entity';
import { ArticleVersion } from '../../database/entities/article-version.entity';
import { ArticleByUserDTO } from '../../common/dto/article/articles-by-user.dto';
import { Article } from '../../database/entities/article.entity';
import { ShowVersionDTO } from '../../common/dto/version/show-version.dto';
import { UserActionDTO } from '../../common/dto/user/request-action-user.dto';
import { UserAction } from '../../common/enum/user-action';
import { Language } from '../../database/entities/language.entity';
import { UpdateUserDTO } from '../../common/dto/user/update-user.dto';
import { LanguageDTO } from '../../common/dto/language/language.dto';

@Injectable()
export class UserService {
  public constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Group) private readonly groupRepo: Repository<Group>,
    @InjectRepository(ArticleVersion) private readonly versionRepo: Repository<ArticleVersion>,
    @InjectRepository(Language) private readonly languageRepo: Repository<Language>,
  ) { }

  public async findUserByUsername(name: string): Promise<User> {
    try {
      const foundUser: User = await this.userRepo.findOneOrFail({
        where: {
          username: name,
          isDeleted: false,
        },
      });
      return foundUser;
    } catch {
      throw new Error('no such User exists!');
    }
  }

  public async allUsers(withoutDeleted: boolean = true) {

    const allUsers = withoutDeleted
      ? await this.userRepo.find({ where: { isDeleted: !withoutDeleted }, relations: ['group'] })
      : await this.userRepo.find({});
    return allUsers;
  }

  public async userById(userId: string) {
    const foundUser: User = await this.userRepo.findOne({
      where: { id: userId },
      relations: ['article', 'languagePreference', 'group'],
    });
    const foundUserArticles: Article[] = await foundUser.article;
    // DTO Mappinf of article
    let returnArticles: ArticleByUserDTO[];
    const returnArticlePromise = foundUserArticles.map(
      async (article: Article) => {
        const versions = await article.versions;
        const currentVersion: ShowVersionDTO = versions.find((articleVersion: ShowVersionDTO) => articleVersion.isCurrent === true);
        await currentVersion.text;
        await currentVersion.title;
        return {
          id: article.id,
          version: currentVersion,
        };
      });
    const returnLanguage = await foundUser.languagePreference;
    const returnGroup = await foundUser.group;
    returnArticles = await Promise.all(returnArticlePromise);
    const returnUser = {
      id: foundUser.id,
      username: foundUser.username,
      article: returnArticles,
      languagePreference: returnLanguage,
      group: returnGroup,
    };
    return returnUser;
  }

  public async createUser(user: UpdateUserDTO) {
    const foundLanguage = await this.languageRepo.findOne({
      where: {
        symbol: user.languagePreference,
      },
    });

    const foundUser: User = await this.userRepo.findOne({
      username: user.username,
    });
    if (foundUser) {
      throw new TranslationSystemError('User with such username already exists!', 400);
    }
    const createUser = {username: user.username, password: user.password};
    let userEntity: User = this.userRepo.create(createUser);
    userEntity.password = await bcrypt.hash(user.password, 10);
    userEntity.languagePreference = Promise.resolve(foundLanguage);

    userEntity = await this.userRepo.save(userEntity);
    this.assignUserToGroup(userEntity, GroupName.contributor);

    return userEntity;
  }

  public async deleteUser(userId: string) {
    const foundUser = await this.userRepo.findOne({ id: userId });
    if (!foundUser) {
      throw new TranslationSystemError(`No user found with id: ${userId}`, 400);
    }
    foundUser.isDeleted = true;
    return await this.userRepo.save(foundUser);
  }

  private async assignUserToGroup(user: User, groupName: GroupName): Promise<Group> {
    const foundGroup = await this.groupRepo.findOne({ where: { name: groupName } });

    const users = await foundGroup.user;
    users.push(user);
    foundGroup.user = Promise.resolve(users);
    return await this.groupRepo.save(foundGroup);
  }

  private async removeUserFromGroup(user: User, groupName: GroupName): Promise<Group> {
    const foundGroup = await this.groupRepo.findOne({ where: { name: groupName }, relations: ['user'] });

    const users = await foundGroup.user;
    const filteredUsers = users.filter((el: User) => el.id !== user.id);
    foundGroup.user = Promise.resolve(filteredUsers);
    return await this.groupRepo.save(foundGroup);
  }

  public async getUserPermissions(userId: string): Promise<Set<string>> {
    const foundUser = await this.userRepo.findOne({
      where: { id: userId },
      relations: ['group'],
    });
    if (!foundUser) {
      throw new TranslationSystemError('User not found', 403);
    }
    const userGroups = await foundUser.group;

    const permissions: Permission[] = await userGroups.reduce(async (accumulator: Promise<Permission[]>, el: Group) => {
      const groupPermissions = await el.permission;
      (await accumulator).push(...groupPermissions);
      return accumulator;
    }, Promise.resolve([]));

    const uniquePermissions = new Set(permissions.map(el => el.name));
    return uniquePermissions;
  }

  public async updateUser(userId: string, body: LanguageDTO): Promise<User> {

    const foundLanguage = await this.languageRepo.findOne({
      where: {
        symbol: body.symbol,
      },
    });
    if (!foundLanguage) {
      throw new TranslationSystemError('Language not found', 403);
    }
    const foundUser = await this.userRepo.findOne({
      where: { id: userId }, relations: ['group', 'languagePreference'],
    });
    if (!foundUser) {
      throw new TranslationSystemError('User not found', 403);
    }
    foundUser.username = foundUser.username;
    foundUser.languagePreference = Promise.resolve(foundLanguage);
    // foundUser.password = await bcrypt.hash(body.password, 10);
    await this.userRepo.save(foundUser);
    return foundUser;
  }

  public async changeUserGroup(userId: string, body: UserActionDTO) {
    let foundUser = await this.userRepo.findOne({
      where: { id: userId },
    });
    if (body.action === UserAction.Remove) {
      const groupToRemove = await this.removeUserFromGroup(foundUser, body.group);
      const newGroups = await foundUser.group;
      foundUser.group = Promise.resolve(newGroups.filter((group: Group) => group.id !== groupToRemove.id));
      return foundUser;
    } else if (body.action === UserAction.Add) {

      const groupToAdd = await this.assignUserToGroup(foundUser, body.group);
      foundUser = await this.userRepo.findOne({
        where: { id: userId }, relations:['group'],
      });
      return foundUser;
    }

  }
}
