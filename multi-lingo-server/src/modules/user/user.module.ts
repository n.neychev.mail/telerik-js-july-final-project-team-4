import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user.service';
import { UserController } from './user.contoller';
import { User } from '../../database/entities/user.entity';
import { Group } from '../../database/entities/group.entity';
import { ArticleVersion } from '../../database/entities/article-version.entity';
import { Language } from '../../database/entities/language.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Group, ArticleVersion, Language])],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule { }
