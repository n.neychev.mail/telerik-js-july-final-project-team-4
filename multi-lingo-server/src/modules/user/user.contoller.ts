import {
    Controller, Post, HttpCode, HttpStatus, Body, UseGuards, Delete, Query, Get, Patch, Param, BadRequestException, UseInterceptors, Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { LogInUserDTO } from '../../common/dto/user/login-user-dto';
import { User } from 'src/database/entities/user.entity';
import { TransformInterceptor } from '../../common/interceptors/transform.interceptor';
import { ReturnSingleUserDTO } from '../../common/dto/user/return-single-user.dto';
import { PermissionGuard } from '../../common/guard/permission.guard';
import { PermissionName } from '../../common/enum/permission.enum';
import { ReturnUpdatedUserDTO } from '../../common/dto/user/return-update-user.dto';
import { UserActionDTO } from '../../common/dto/user/request-action-user.dto';
import { UserAction } from '../../common/enum/user-action';
import { ReturnAllUserDTO } from '../../common/dto/user/return-all-user.dto';
import { UpdateUserDTO } from '../../common/dto/user/update-user.dto';
import { LanguageDTO } from '../../common/dto/language/language.dto';

@Controller('/users')
@ApiUseTags('User Controller')
export class UserController {

    public constructor(
        private readonly userService: UserService,
    ) { }

    @Get('')
    @UseInterceptors(new TransformInterceptor(ReturnAllUserDTO))
    public async allUsers() {
        return await this.userService.allUsers();
    }
    @Get(':id')
    @UseInterceptors(new TransformInterceptor(ReturnSingleUserDTO))

    public async findUserById(@Param('id') id: string) {
        return await this.userService.userById(id);
    }
    @Post()
    public async addNewUser(
        @Body() user: UpdateUserDTO,
        ) {
        return await this.userService.createUser(user);
    }
    @Delete(':id')
    public async deleteUser(@Param('id') id: string): Promise<User> {
        return await this.userService.deleteUser(id);
    }

    @Get(':id/permissions')
    public async getUserPermissions(
        @Param('id') id: string): Promise<string[]> {
        const userPermissions = await this.userService.getUserPermissions(id);
        return [...userPermissions];
    }

    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(new TransformInterceptor(ReturnSingleUserDTO))
    @Patch(':id/language')
    public async updateUserLanguage(
        @Param('id') id: string,
        @Body() body: LanguageDTO,
    ): Promise<ReturnUpdatedUserDTO> {
        return await this.userService.updateUser(id, body);
    }

    @UseGuards(AuthGuard('jwt'), PermissionGuard(PermissionName.Update_User))
    @UseInterceptors(new TransformInterceptor(ReturnAllUserDTO))
    @Patch(':id/group')
    public async updateUserGroup(
        @Param('id') id: string,
        @Body() body: UserActionDTO,
    ) {
        return await this.userService.changeUserGroup(id, body);
    }
}
