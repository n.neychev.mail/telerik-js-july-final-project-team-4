import {
    Controller, Post, Get, Delete, Patch, Query, Body, Param, Put, ValidationPipe, Req, UseGuards, UseInterceptors, BadRequestException, UsePipes,
} from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { ArticleService } from './article.service';
import { CreateArticleDTO } from '../../common/dto/article/create-article-dto';
import { Article } from 'src/database/entities/article.entity';
import { TransformInterceptor } from '../../common/interceptors/transform.interceptor';
import { ReturnAllArticleVersionDTO } from '../../common/dto/article/return-all-article-version.dto';
import { RequestUser } from '../../common/decorator/user.decorator';
import { RequestUserDTO } from '../../common/dto/user/request-user.dto';
import { AuthGuard } from '@nestjs/passport';
import { Language } from '../../database/entities/language.entity';
@Controller('articles')
@ApiUseTags('Article Controller')
export class ArticleController {
    public constructor(
        private readonly articleService: ArticleService) { }

    @Get('')
    @UseInterceptors(new TransformInterceptor(ReturnAllArticleVersionDTO))
    public async allArticles(
        @Query('language') language: string,
    ) {
        return await this.articleService.allArticles(language);
    }

    @Get(':id')
    public async articleById(@Param('id') id: string) {
        return await this.articleService.articleById(id);
    }

    @Post()
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    public async createArticle(
        @Body() article: CreateArticleDTO,
        @RequestUser('user') user: RequestUserDTO) {
        return await this.articleService.createArticle(article, user);
    }

    @Delete(':id')
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))

    public async deleteArticle(
        @Param('id') id: string,
        @RequestUser('user') user: RequestUserDTO) {
        return await this.articleService.deleteArticle(id,user);
    }

    @Put(':id')
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    public async updateArticle(
        @Param('id') id: string,
        @Body() article: CreateArticleDTO,
        @RequestUser('user') user: RequestUserDTO,
        ) {
        return await this.articleService.updateArticle(id, article, user);
    }

    @Patch('/version/:versionId')
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    public async changeCurrentVersion(
        @Param('versionId') id: string,
    ) {
        return await this.articleService.changedVersion(id);
    }
}
