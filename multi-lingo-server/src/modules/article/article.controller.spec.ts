import { ArticleController } from './article.contoller';
import { TestingModule, Test } from '@nestjs/testing';
import { ArticleService } from './article.service';
import { PassportModule } from '@nestjs/passport';
import { CreateArticleDTO } from '../../common/dto/article/create-article-dto';
import { RequestUserDTO } from '../../common/dto/user/request-user.dto';

describe('Article controller', () => {
    let controller: ArticleController;
    let service: any;
    beforeEach(async () => {
        service = {
            allArticles() { },
            articleById() { },
           createArticle() {},
            deleteArticle() {},
            updateArticle() {},
            changedVersion() { },
        };
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ArticleController],
            providers: [
                {
                    provide: ArticleService,
                    useValue: service,
                },
            ],
            imports: [PassportModule.register({defaultStrategy: 'jwt'})],
        }).compile();
        controller = module.get<ArticleController>(ArticleController);
    });
    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
    describe('AllArticles', () => {
        it('should call articleService allArticles  with correct param', async () => {
            const spy = jest.spyOn(service, 'allArticles');
            const lang = 'en';
            await controller.allArticles(lang);
            expect(spy).toBeCalledWith(lang);
            expect(spy).toBeCalledTimes(1);
        });
    });
    describe('get one article by id', () => {
        it('should call article service articleById once', async () => {
            const fakeArtId = '1';
            const spy = jest.spyOn(service, 'articleById');
            await controller.articleById(fakeArtId);
            expect(spy).toBeCalledWith(fakeArtId);
            expect(spy).toBeCalledTimes(1);
        });
    });
    describe('addNewArticle ', () => {
        it('should call article service add new article once', async () => {
            const fakeArt = new CreateArticleDTO();
            const user = new RequestUserDTO();
            const spy = jest.spyOn(service, 'createArticle');
            const result = await controller.createArticle(fakeArt, user);
            expect(spy).toBeCalledWith(fakeArt, user);
        });
    });
    describe('deleteArticle', () => {
        it('should call article service deleteArticle', async () => {
            const artId = '1';
            const spy = jest.spyOn(service, 'deleteArticle');
            await controller.deleteArticle(artId);
            expect(spy).toBeCalledWith(artId);
        });
    });
    describe('update article ', () => {
        it('should call article service updateArticle', async () => {
            const id = '1';
            const article = new CreateArticleDTO();
            const spy = jest.spyOn(service, 'updateArticle');
            await controller.updateArticle(id, article);
            expect(spy).toBeCalledWith(id, article);
        });
    });
    describe('change current version ', () => {
        it('should call article service updateArticle', async () => {
            const id = '1';
            const spy = jest.spyOn(service, 'changedVersion');
            await controller.changeCurrentVersion(id);
            expect(spy).toBeCalledWith(id);
        });
    });
});
