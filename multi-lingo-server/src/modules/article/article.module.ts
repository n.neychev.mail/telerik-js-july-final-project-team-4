import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleController } from './article.contoller';
import { ArticleService } from './article.service';
import { Article } from '../../database/entities/article.entity';
import { Phrase } from '../../database/entities/phrase.entity';
import { ArticleVersion } from '../../database/entities/article-version.entity';
import { PhraseService } from '../../core/services/phrase.service';
import { Language } from '../../database/entities/language.entity';
import { TranslationService } from '../translation/translation.service';
import { Translation } from '../../database/entities/translation.entity';
import { User } from '../../database/entities/user.entity';
import { Rate } from '../../database/entities/rating.entity';
import { LanguageService } from '../language/language.service';
import { UiComponent } from '../../database/entities/ui-component.entity';
import { UiComponentService } from '../ui-components/ui-component.service';
import { UiTranslation } from '../../database/entities/ui-translation.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Article, Phrase, ArticleVersion, Language, Translation, User, Rate, UiComponent, UiTranslation])],
  controllers: [ArticleController],
  providers: [ArticleService, PhraseService, TranslationService, LanguageService, UiComponentService],
})
export class ArticleModule { }
