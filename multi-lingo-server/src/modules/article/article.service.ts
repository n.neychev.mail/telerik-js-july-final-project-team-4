import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Injectable, Inject } from '@nestjs/common';
import { Article } from '../../database/entities/article.entity';
import { Phrase } from '../../database/entities/phrase.entity';
import { CreateArticleDTO } from '../../common/dto/article/create-article-dto';
import { ArticleVersion } from '../../database/entities/article-version.entity';
import { TranslationSystemError } from '../../common/exceptions/multilingo-system-error';
import { PhraseService } from '../../core/services/phrase.service';
import { User } from '../../database/entities/user.entity';
import { RequestUserDTO } from '../../common/dto/user/request-user.dto';
import { Language } from '../../database/entities/language.entity';
import { ReturnVersionDTO } from '../../common/dto/version/return-version.dto';
import { PhraseDTO } from '../../common/dto/phase/phrase.dto';
import { TranslationService } from '../translation/translation.service';
import { LanguageService } from '../language/language.service';
@Injectable()
export class ArticleService {
    public constructor(
        @InjectRepository(Article) private readonly articleRepo: Repository<Article>,
        @InjectRepository(ArticleVersion) private readonly versionRepo: Repository<ArticleVersion>,
        @Inject(PhraseService) private readonly phraseService: PhraseService,
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @Inject(TranslationService) private readonly translationService: TranslationService,
        @Inject(LanguageService) private readonly languageService: LanguageService,

    ) { }

    public async allArticles(languageSymbol: string) {
        const foundLanguage = await this.languageService.getLanguage(languageSymbol);
        const allCurrentVersion: ArticleVersion[] = await this.versionRepo.find({
            where: {
                isCurrent: true,
                relations: ['article', 'title', 'text'],
            },
        });
        const returnVersions: ReturnVersionDTO[] = await Promise.all(allCurrentVersion.map(async (x) => {
            const translationLanguage = foundLanguage;
            const pharse: Phrase = await x.text;
            const titlePhrase: Phrase = await x.title;
            const usePhrase = (pharse.translations.filter(sinlgePhrase => sinlgePhrase.language.symbol === translationLanguage.symbol));
            const useTitlePhrase = (titlePhrase.translations.filter(sinlgePhrase => sinlgePhrase.language.symbol === translationLanguage.symbol));
            usePhrase[0].rates = await this.translationService.calculateTranslationRatings(usePhrase[0]);
            x.title = Promise.resolve(titlePhrase);
            x.text = Promise.resolve(pharse);

            const textPhraseDTO: PhraseDTO = {
                id: pharse.id,
                text: pharse.text,
                translations: usePhrase[0],
            };
            const titlePhraseDTO: PhraseDTO = {
                id: titlePhrase.id,
                text: titlePhrase.text,
                translations: useTitlePhrase[0],
            };
            const returnVersion: ReturnVersionDTO = {
                id: x.id,
                number: x.number,
                isCurrent: x.isCurrent,
                article: await x.article,
                title: titlePhraseDTO,
                text: textPhraseDTO,
            };
            return returnVersion;
        }));
        return returnVersions;

    }
    public async articleById(articleId: string) {
        const foundArticle: Article = await this.articleRepo.findOneOrFail({
            where: {
                id: articleId,
            }, relations: ['versions'],
        });
        const version: ArticleVersion = (foundArticle as any).__versions__;
        return {
            id: foundArticle.id,
            isDeleted: foundArticle.isDeleted,
            versions: version,
        };
    }
    public async createArticle(article: CreateArticleDTO, user: RequestUserDTO) {
        const foundTitle = await this.phraseService.checkOrCreate(article.title);
        const foundPhrase = await this.phraseService.checkOrCreate(article.text);
        const foundUser = await this.userRepo.findOne({
            where: {
                id: user.id,
            },
        });
        const createVersion: ArticleVersion = this.versionRepo.create();
        createVersion.title = Promise.resolve(foundTitle);
        createVersion.text = Promise.resolve(foundPhrase);
        const versionEntity = await this.versionRepo.save(createVersion);
        const articleEntity: Article = this.articleRepo.create();
        articleEntity.versions = Promise.resolve([versionEntity]);
        articleEntity.user = Promise.resolve(foundUser);
        return await this.articleRepo.save(articleEntity);
    }
    public async deleteArticle(articleId: string, user: RequestUserDTO) {
        const foundUser = await this.userRepo.findOne({
            where: {
                id: user.id,
            }, relations: ['group', 'article']
        }
        )
        const foundUsergroup = await foundUser.group;
        const foundUserArticles = await foundUser.article;
        if (foundUsergroup.some(element => element.name === 'Admin' || foundUserArticles.some((element) => element.id === articleId))) {
            const foundArticle = await this.articleRepo.findOne({ id: articleId });
            if (!foundArticle) {
                throw new TranslationSystemError(`No article found with id: ${articleId}`, 404);
            }
            return await this.articleRepo.delete(foundArticle);
        } else {
            throw new TranslationSystemError(`You do not have permission to delete this article!!!}`, 403);

        }
    }
    public async updateArticle(articleId: string, article: CreateArticleDTO, user: RequestUserDTO) {

        const foundUser = await this.userRepo.findOne({
            where: {
                id: user.id,
            }, relations: ['group']
        },
        )
        const foundUsergroup = await foundUser.group;
        if (!foundUsergroup.some(element => element.name === 'Admin')) {
            throw new TranslationSystemError(`You do not have permission to update this article!!!}`, 403);
        }
        const foundArticle = await this.articleRepo.findOneOrFail({
            where: {
                id: articleId,
            },
        });
        const foundVersions: ArticleVersion[] = await this.versionRepo.find({
            where: {
                article: foundArticle.id,
            }, relations: ['article'],
        });
        const versionArray: ArticleVersion[] = foundVersions.filter(x => x.isCurrent === true);
        const currentVersion = versionArray[0];
        currentVersion.isCurrent = false;
        await this.versionRepo.save(currentVersion);
        const foundTitle = await this.phraseService.checkOrCreate(article.title);
        const foundPhrase = await this.phraseService.checkOrCreate(article.text);

        const createVersion: ArticleVersion = this.versionRepo.create();
        createVersion.title = Promise.resolve(foundTitle);
        createVersion.text = Promise.resolve(foundPhrase);
        createVersion.number = foundVersions.length + 1;
        const versionEntity = await this.versionRepo.save(createVersion);
        const articleVersions = await foundArticle.versions;
        articleVersions.push(createVersion);
        foundArticle.versions = Promise.resolve(articleVersions);
        return await this.articleRepo.save(foundArticle);
    }

    public async changedVersion(versionId: string) {
        const newVersion = await this.versionRepo.findOne({
            where: {
                id: versionId,
            },
            relations: ['article'],
        });
        const foundArticle = await newVersion.article;
        const oldVersion = await this.versionRepo.findOne({
            where: {
                article: foundArticle,
                isCurrent: true,
            },
            relations: ['article'],
        });
        oldVersion.isCurrent = false;
        await this.versionRepo.save(oldVersion);
        newVersion.isCurrent = true;
        await this.versionRepo.save(newVersion);
        return foundArticle.versions;
    }
}
