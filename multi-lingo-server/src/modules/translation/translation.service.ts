import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ConflictException, Injectable, HttpException, NotFoundException, ForbiddenException, BadRequestException, Inject } from '@nestjs/common';
import { Translation } from '../../database/entities//translation.entity';
import { translate } from 'google-translate-api-browser';
import { Phrase } from '../../database/entities/phrase.entity';
import { Language } from '../../database/entities/language.entity';
import { GoogleTranslateDTO } from 'src/common/dto/translation/google-translate/google-translate.dto';
import { CreateTranslationDTO } from 'src/common/dto/translation/create-translation.dto';

import { UpdateTranslationDTO } from 'src/common/dto/translation/update-translation.dto';
import { TranslationSystemError } from '../../common/exceptions/multilingo-system-error';
import { Rate } from '../../database/entities/rating.entity';
import { TranslatePhraseDTO } from '../../common/dto/translation/translate-phrase.dto';

export class TranslationService {
    public constructor(
        @InjectRepository(Translation) private readonly translationRepo: Repository<Translation>,
        @InjectRepository(Language) private readonly languageRepo: Repository<Language>,
        @InjectRepository(Phrase) private readonly phraseRepo: Repository<Phrase>,
        @InjectRepository(Rate) private readonly rateRepo: Repository<Rate>,

    ) { }

    public async allTranslations(): Promise<Translation[]> {
        const allTranslations: Translation[] = await this.translationRepo.find({ relations: ['rates', 'origin'] });
        return allTranslations;
    }

    public async createTranslation(phraseId: string, languageId: string): Promise<Translation> {

        const foundPhrase: Phrase = await this.phraseRepo.findOne({
            where: {
                id: phraseId,
                relations: ['translatations'],
            },
        });
        const foundLanguage: Language = await this.languageRepo.findOne({
            where: {
                id: languageId,
            },
        });
        const foundTranslations = await foundPhrase.translations;
        if (foundTranslations.some(el => el.language.id === languageId)) {
            const singleTranslation: Translation = foundTranslations.find(x => x.language.id === foundLanguage.id);
            return singleTranslation;
        }
        const translatedPhrase = await translate(foundPhrase.text, { to: foundLanguage.symbol })
            .then((res: GoogleTranslateDTO) => res.text)
            .catch(err => {
                throw new Error(err);
            });
        const newTranslation: CreateTranslationDTO = {
            text: translatedPhrase,
            origin: foundPhrase,
            language: foundLanguage,
            rates: Promise.resolve(new Array<Rate>()),
        };
        let createdTranslation = this.translationRepo.create(newTranslation);
        createdTranslation = await this.translationRepo.save(createdTranslation);
        return createdTranslation;
    }

    public async updateTranslation(translateId: string, body: UpdateTranslationDTO): Promise<Translation> {
        const foundTranslation: Translation = await this.translationRepo.findOne(translateId);
        if (!foundTranslation) {
            throw new Error('Not found');
        }
        foundTranslation.text = body.text;
        await this.translationRepo.save(foundTranslation);
        return foundTranslation;
    }
    public async rateTranslations(value: number, textId: string) {
        const foundText = await this.translationRepo.findOne({
            where: {
                id: textId,
            },
            relations: ['rates'],
        });
        if (!foundText) {
            throw new TranslationSystemError('No such translation exists', 404);
        }

        const rateToCreate = {
            amount: value,
        };
        let returnRating = this.rateRepo.create(rateToCreate);
        returnRating.translations = Promise.resolve(foundText);
        returnRating = await this.rateRepo.save(returnRating);
        return returnRating;

    }
    public async calculateTranslationRatings(foundTranslation: Translation) {
        const transRatings = await foundTranslation.rates;
        const sum = transRatings.reduce((accumulator, currValue) => {
            accumulator = accumulator + (+currValue.amount);
            return accumulator;
        }, 0);

        let avg;
        avg = (sum / transRatings.length).toFixed(2);
        return avg === 'NaN' ? '0' : avg;
    }

}
