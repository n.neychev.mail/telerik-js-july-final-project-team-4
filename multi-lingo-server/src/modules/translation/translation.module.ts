import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TranslationController } from './translation.contoller';
import { TranslationService } from './translation.service';
import { Translation } from '../../database/entities/translation.entity';
import { Language } from '../../database/entities/language.entity';
import { Phrase } from '../../database/entities/phrase.entity';
import { Rate } from '../../database/entities/rating.entity';
import { UserService } from '../user/user.service';
import { User } from '../../database/entities/user.entity';
import { Group } from '../../database/entities/group.entity';
import { ArticleVersion } from '../../database/entities/article-version.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Translation, Language, Phrase, Rate, User, Group, ArticleVersion])],
  controllers: [TranslationController],
  providers: [TranslationService, UserService],
  exports: [TranslationService],
})
export class TranslationModule {}
