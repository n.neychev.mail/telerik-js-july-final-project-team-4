// tslint:disable: prefer-const
import { ArticleService } from "./article.service";
import { any } from "@hapi/joi";
import { TestingModule, Test } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Article } from "../../database/entities/article.entity";
import { Phrase } from "../../database/entities/phrase.entity";
import { User } from "../../database/entities/user.entity";
import { Language } from "../../database/entities/language.entity";
import { ArticleVersion } from "../../database/entities/article-version.entity";
import { PhraseService } from "../../core/services/phrase.service";
import { TranslationService } from "./translation.service";
import { Translation } from "../../database/entities/translation.entity";
import { Rate } from "../../database/entities/rating.entity";
import { ArticleDTO } from "../../common/dto/article/artictle-dto";
import { CreateArticleDTO } from "../../common/dto/article/create-article-dto";
import { RequestUserDTO } from "../../common/dto/user/request-user.dto";
import { EPERM } from "constants";

describe('article service ', () => {
    let service: ArticleService;
    let phraseService: PhraseService;
    let translationService: TranslationService;
    let articleRepo: any;
    let phraseRepo: any;
    let versionRepo: any;
    let languageRepo: any;
    let userRepo: any;
    let translationRepo: any;
    let rateRepo: any;
    beforeEach(async () => {
        articleRepo = {
            find() {
                /* empty */
            },
            findOne() {
                /* empty */
            },
            create() {
                /* empty */
            },
            save() {
                /* empty */
            },
            findOneOrFail() {

            },
            delete() {

            }
        },
            phraseRepo = {
                find() {
                    /* empty */
                },
                findOne() {
                    /* empty */
                },
                create() {
                    /* empty */
                },
                save() {
                    /* empty */
                },
            },
            versionRepo = {
                find() {
                    /* empty */
                },
                findOne() {
                    /* empty */
                },
                create() {
                    /* empty */
                },
                save() {
                    /* empty */
                },
            },
            languageRepo = {
                find() {
                    /* empty */
                },
                findOne() {
                    /* empty */
                },
                create() {
                    /* empty */
                },
                save() {
                    /* empty */
                },
            },
            userRepo = {
                find() {
                    /* empty */
                },
                findOne() {
                    /* empty */
                },
                create() {
                    /* empty */
                },
                save() {
                    /* empty */
                },
            };
        translationRepo = {
            find() {
                /* empty */
            },
            findOne() {
                /* empty */
            },
            create() {
                /* empty */
            },
            save() {
                /* empty */
            },
        }
        rateRepo = {
            find() {
                /* empty */
            },
            findOne() {
                /* empty */
            },
            create() {
                /* empty */
            },
            save() {
                /* empty */
            },
        };

        const module: TestingModule = await Test.createTestingModule({
            providers: [
                ArticleService, PhraseService, TranslationService,
                { provide: getRepositoryToken(Article), useValue: articleRepo },
                { provide: getRepositoryToken(Phrase), useValue: phraseRepo },
                { provide: getRepositoryToken(User), useValue: userRepo },
                { provide: getRepositoryToken(Language), useValue: languageRepo },
                { provide: getRepositoryToken(ArticleVersion), useValue: versionRepo },
                { provide: getRepositoryToken(Translation), useValue: translationRepo },
                { provide: getRepositoryToken(Rate), useValue: rateRepo },
            ],
            imports: []
        }).compile()
        service = module.get<ArticleService>(ArticleService)
    })
    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });
    describe('All articles ', () => {
        it('should  call language Repo with correct language', async () => {
            const spy = jest
                .spyOn(languageRepo, 'findOne')
                .mockReturnValue(Promise.resolve())
            const spyLanguage = jest
                .spyOn(versionRepo, 'find')
                .mockReturnValue(Promise.resolve([]));
            const expectedLanguage = 'en';
            const result = await service.allArticles(expectedLanguage);
            expect(spy).toBeCalledWith({ where: { symbol: expectedLanguage } });
            expect(spy).toBeCalledTimes(1);
            expect(spyLanguage).toBeCalledWith({
                where: {
                    isCurrent: true,
                    relations: ['article', 'title', 'text'],
                },
            })
        })

    })
    describe('article by id ', () => {
        it('should call article repo with correct id', async () => {
            const fakeArticle = {
                id: '1'
            }
            const spy = jest
                .spyOn(articleRepo, 'findOneOrFail')
                .mockReturnValue(Promise.resolve(fakeArticle))
            const result = await service.articleById(fakeArticle.id)
            expect(spy).toBeCalledWith({
                where: {
                    id: '1',
                }, relations: ['versions'],
            })

        })
    })
    // describe('create article',()=>{
    //     it('should create an article ', async()=>{
    //         const fakeArticle = {
    //             title: 'ne6to',
    //             text: 'tam'
    //         }
    //         const user = new RequestUserDTO()
    //         const spy = jest
    //         .spyOn(userRepo,'findOne')
    //         .mockReturnValue(Promise.resolve(user))
    //         // const spy = jest 
    //         // .spyOn(phraseService,'checkOrCreate')
    //         // .mockReturnValue(Promise.resolve(fakeArticle.text))
    //         const result = await service.createArticle(fakeArticle, user)
    //         // expect(phraseService.checkOrCreate).toBeCalledWith(fakeArticle.text)
    //         // expect(phraseService.checkOrCreate).toBeCalledWith(fakeArticle.title)
    //         expect(spy).toHaveBeenCalledWith(user)
    //     })
    // })
    describe('delete article ', () => {
        it('delete the article', async () => {
            const mockArticle = {
                id: '1'
            }
            const spy = jest.spyOn(articleRepo, 'findOne')
                .mockReturnValue(Promise.resolve(mockArticle))
            const deleteSpy = jest
                .spyOn(articleRepo, 'delete')
                .mockReturnValue(null)
            const result = await service.deleteArticle(mockArticle.id)
            expect(spy).toBeCalledWith({ id: mockArticle.id })
            expect(deleteSpy).toBeCalledWith(mockArticle)
        })
    })
})