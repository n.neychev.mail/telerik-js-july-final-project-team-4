import {
  Controller, Post, Get, Patch, Body, Param, Put, UseGuards, UseInterceptors,
} from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { TranslationService } from './translation.service';
import { AuthGuard } from '@nestjs/passport';
import { RequestUser } from '../../common/decorator/user.decorator';
import { PayloadDTO } from '../../core/auth/payload.dto';
import { ReturnTranslationDTO } from '../../common/dto/translation/return-translation.dto';
import { Translation } from 'src/database/entities/translation.entity';
import { UpdateTranslationDTO } from '../../common/dto/translation/update-translation.dto';
import { TranslatePhraseDTO } from '../../common/dto/translation/translate-phrase.dto';
import { TransformInterceptor } from '../../common/interceptors/transform.interceptor';
import { ReturnAllTranslationDTO } from '../../common/dto/translation/return-all-translation.dto';
import { PermissionGuard } from '../../common/guard/permission.guard';
import { PermissionName } from '../../common/enum/permission.enum';

@Controller('translations')
@ApiUseTags('Translation Controller')
export class TranslationController {
  public constructor(
    private readonly translationService: TranslationService,
  ) { }

  @Get()
  @UseInterceptors(new TransformInterceptor(ReturnAllTranslationDTO))
  @UseGuards(AuthGuard('jwt'), PermissionGuard(PermissionName.Read_Translation))
  @ApiBearerAuth()
  public async allTranslations(@RequestUser() user: PayloadDTO): Promise<ReturnAllTranslationDTO[]> {
    const foundTranslations: Translation[] = await this.translationService.allTranslations();
    return foundTranslations;
  }
  @Post()
  public async createTranslation(@Body() translation: TranslatePhraseDTO) {
    const createdTranslation = this.translationService.createTranslation(translation.phraseId, translation.languageId);
    return createdTranslation;
  }

  @Put('/:translationId')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  public async updateTranslation(
    @Param('translationId') translationId: string,
    @Body() body: UpdateTranslationDTO,
  ): Promise<ReturnTranslationDTO> {
    const updatedTranslation: Translation = await this.translationService.updateTranslation(translationId, body);
    return updatedTranslation;
  }
  @Patch('/:translationId')
  public async rateTranslation(
    @Param('translationId') translationId: string,
    @Body() body) {
    const textRating = await this.translationService.rateTranslations(body.value, body.textId);
    const titleRating = await this.translationService.rateTranslations(body.value, body.titleId);
    return [textRating, titleRating];
  }

}
