import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Language } from '../../database/entities/language.entity';
import { LanguageController } from './language.controller';
import { LanguageService } from './language.service';
import { Phrase } from '../../database/entities/phrase.entity';
import { TranslationService } from '../translation/translation.service';
import { Translation } from '../../database/entities/translation.entity';
import { Rate } from '../../database/entities/rating.entity';
import { UiComponent } from '../../database/entities/ui-component.entity';
import { UiTranslation } from '../../database/entities/ui-translation.entity';
import { UiComponentService } from '../ui-components/ui-component.service';

@Module({
  imports: [TypeOrmModule.forFeature([Language, Phrase, Translation, Rate, UiComponent, UiTranslation])],
  controllers: [LanguageController],
  providers: [LanguageService, TranslationService, UiComponentService],
})
export class LanguageModule { }
