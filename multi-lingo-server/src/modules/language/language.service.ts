import { InjectRepository } from '@nestjs/typeorm';
import { Language } from '../../database/entities/language.entity';
import { Repository } from 'typeorm';
import { CreateLanguageDTO } from '../../common/dto/language/create-language.dto';
import { TranslationSystemError } from '../../common/exceptions/multilingo-system-error';
import { Phrase } from '../../database/entities/phrase.entity';
import { TranslationService } from '../translation/translation.service';
import { Injectable } from '@nestjs/common';
import { UiComponent } from '../../database/entities/ui-component.entity';
import { UiComponentService } from '../ui-components/ui-component.service';

@Injectable()
export class LanguageService {
    public constructor(
        @InjectRepository(Language) private readonly languageRepo: Repository<Language>,
        @InjectRepository(Phrase) private readonly phraseRepo: Repository<Phrase>,
        @InjectRepository(UiComponent) private readonly uiComponentRepo: Repository<UiComponent>,
        private readonly translationService: TranslationService,
        private readonly uiComponentService: UiComponentService,
    ) { }
    public async allLanguages(): Promise<Language[]> {
        const allLanguages: Language[] = await this.languageRepo.find();
        return allLanguages;
    }
    public async createLanguage(language: CreateLanguageDTO) {
        const foundLanguage = await this.languageRepo.findOne({ name: language.name });
        if (foundLanguage) {
            throw new TranslationSystemError(`language already exists`, 400);
        }
        let languageEntity: Language = this.languageRepo.create(language);
        languageEntity.name = language.name;
        languageEntity.symbol = language.symbol;
        const allPhrases: Phrase[] = await this.phraseRepo.find();
        const allComponents: UiComponent[] = await this.uiComponentRepo.find();
        languageEntity = await this.languageRepo.save(languageEntity);
        await Promise.all(allComponents.map(async element => await this.uiComponentService.createUiTranslation(element, languageEntity.id)));
        await Promise.all(allPhrases.map(async element => await this.translationService.createTranslation(element.id, languageEntity.id)));
        return languageEntity;
    }

    public async getLanguage(languageSymbol: string): Promise<Language> {
        let foundLanguage = await this.languageRepo.findOne({
            where: {
                symbol: languageSymbol,
            },
        });
        if (!foundLanguage) {
            foundLanguage = await this.languageRepo.findOne({
                where: {
                    name: 'English',
                },
            });
        }
        return foundLanguage;
    }
}
