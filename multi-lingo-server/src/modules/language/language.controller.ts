import { Get, Controller, Post, Body, UseGuards } from '@nestjs/common';
import { LanguageService } from './language.service';
import { CreateLanguageDTO } from '../../common/dto/language/create-language.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('languages')
export class LanguageController {
    public constructor(private readonly languageService: LanguageService) { }
    @Get('')
    public async getAllLanguages() {
        const foundLanguages = await this.languageService.allLanguages();
        return foundLanguages;
    }
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    public async createLanguage(@Body() language: CreateLanguageDTO) {
        return await this.languageService.createLanguage(language);
    }
}
