import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { translate } from 'google-translate-api-browser';
import { Language } from '../../database/entities/language.entity';
import { GoogleTranslateDTO } from 'src/common/dto/translation/google-translate/google-translate.dto';

import { UpdateTranslationDTO } from 'src/common/dto/translation/update-translation.dto';
import { UiComponent } from '../../database/entities/ui-component.entity';
import { ReturnComponentDTO } from '../../common/dto/ui-component/return-component.dto';
import { UiTranslation } from '../../database/entities/ui-translation.entity';
import { CreateComponentTranslationDTO } from '../../common/dto/ui-component/create-component-translation.dto';

export class UiComponentService {
    public constructor(
        @InjectRepository(UiComponent) private readonly uiComponentRepo: Repository<UiComponent>,
        @InjectRepository(UiTranslation) private readonly uiTranslationRepo: Repository<UiTranslation>,
        @InjectRepository(Language) private readonly languageRepo: Repository<Language>,

    ) { }

    public async allComponents(languageSymbol: string, components: string[]): Promise<ReturnComponentDTO[]> {
        let foundLanguage = await this.languageRepo.findOne({
            where: {
                symbol: languageSymbol,
            },
        });
        if (!foundLanguage) {
            foundLanguage = await this.languageRepo.findOne({
                where: {
                    name: 'English',
                },
            });
        }
        const allComponents: UiComponent[] = await this.uiComponentRepo.find({
            where: { text: In(components) },
        });

        const returnComponents: ReturnComponentDTO[] = allComponents.map(component => {
            const translations = component.translations.find(translation => translation.language.id === foundLanguage.id);
            return {
                ...component,
                translations,
            };
        });
        return returnComponents;
    }

    public async updateComponent(translateId: string, body: UpdateTranslationDTO): Promise<UiTranslation> {
        const foundTranslation: UiTranslation = await this.uiTranslationRepo.findOne(translateId);
        if (!foundTranslation) {
            throw new Error('Not found');
        }
        foundTranslation.text = body.text;
        await this.uiTranslationRepo.save(foundTranslation);
        return foundTranslation;
    }
    public async createComponent(componentText: string): Promise<UiComponent> {
        const newComponent = {
            text: componentText,
        };
        let createComponent = this.uiComponentRepo.create(newComponent);
        createComponent = await this.uiComponentRepo.save(createComponent);

        const languageArr = await this.languageRepo.find();
        await Promise.all(languageArr.map((el: Language) => this.createUiTranslation(createComponent, el.id)));
        return createComponent;
    }

    public async createUiTranslation(uiComponent: UiComponent, languageId: string): Promise<UiTranslation> {

        const foundLanguage: Language = await this.languageRepo.findOne({
            where: {
                id: languageId,
            },
        });
        const translatedComponent = await translate(uiComponent.text, { to: foundLanguage.symbol })
            .then((res: GoogleTranslateDTO) => res.text)
            .catch(err => {
                throw new Error(err);
            });
        const newTranslation: CreateComponentTranslationDTO = {
            text: translatedComponent,
            uiComponent,
            language: foundLanguage,
        };
        let createdTranslation = this.uiTranslationRepo.create(newTranslation);
        createdTranslation = await this.uiTranslationRepo.save(createdTranslation);
        return createdTranslation;
    }
}
