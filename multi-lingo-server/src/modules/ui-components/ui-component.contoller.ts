import {
  Controller, Post, Get, Delete, Patch, Query, Body, Param, Put, ValidationPipe, Req, UseGuards, UseInterceptors, BadRequestException,
} from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { UiComponentService } from './ui-component.service';
import { AuthGuard } from '@nestjs/passport';
import { ReturnTranslationDTO } from '../../common/dto/translation/return-translation.dto';
import { UpdateTranslationDTO } from '../../common/dto/translation/update-translation.dto';
import { TransformInterceptor } from '../../common/interceptors/transform.interceptor';
import { ReturnAllTranslationDTO } from '../../common/dto/translation/return-all-translation.dto';
import { ReturnComponentDTO } from '../../common/dto/ui-component/return-component.dto';
import { CreateComponentDTO } from '../../common/dto/ui-component/create-component.dto';
import { UiTranslation } from '../../database/entities/ui-translation.entity';
import { PermissionGuard } from '../../common/guard/permission.guard';
import { PermissionName } from '../../common/enum/permission.enum';

@Controller('ui-component')
@ApiUseTags('UI Component Controller')
export class UiComponentController {
  public constructor(
    private readonly uiComponentService: UiComponentService,
  ) { }

  @Get()
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ReturnComponentDTO))
  @UseGuards(AuthGuard('jwt'))
  public async allComponents(
    @Query('language') language: string,
    @Query('componentStrings') components: string,
  ): Promise<ReturnComponentDTO[]> {
    const componentTexts: string[] = components.split(',');
    const foundTranslations: ReturnComponentDTO[] = await this.uiComponentService.allComponents(language, componentTexts);
    return foundTranslations;
  }

  @Post()
  public async createComponent(@Body() body: CreateComponentDTO) {
    console.log(body);
    const createdComponent = this.uiComponentService.createComponent(body.text);
    return createdComponent;
  }

  @Put('/:componentId')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  public async updateComponent(
    @Param('componentId') componentId: string,
    @Body() body: CreateComponentDTO,
  ): Promise<ReturnTranslationDTO> {
    const updatedComponent: UiTranslation = await this.uiComponentService.updateComponent(componentId, body);
    return updatedComponent;
  }
}
