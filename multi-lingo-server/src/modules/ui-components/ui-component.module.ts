import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Language } from '../../database/entities/language.entity';
import { UiComponentController } from './ui-component.contoller';
import { UiComponentService } from './ui-component.service';
import { UiComponent } from '../../database/entities/ui-component.entity';
import { UiTranslation } from '../../database/entities/ui-translation.entity';
import { LanguageService } from '../language/language.service';
import { Phrase } from '../../database/entities/phrase.entity';
import { TranslationService } from '../translation/translation.service';
import { Translation } from '../../database/entities/translation.entity';
import { Rate } from '../../database/entities/rating.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UiTranslation, Language, UiComponent, Phrase, Translation, Rate])],
  controllers: [UiComponentController],
  providers: [UiComponentService, LanguageService, TranslationService],
  exports: [],
})
export class UiComponentModule {}
