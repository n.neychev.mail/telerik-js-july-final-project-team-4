import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './modules/user/user.module';
import { ArticleModule } from './modules/article/article.module';
import { APP_FILTER } from '@nestjs/core';
import { MultilingoSystemErrorFilter } from './common/filter/multilingo-error.filter';
import { LanguageModule } from './modules/language/language.module';
import { TranslationModule } from './modules/translation/translation.module';
import { AuthModule } from './core/auth/auth.module';
import { UiComponentModule } from './modules/ui-components/ui-component.module';

@Module({
  imports: [DatabaseModule, UserModule, ArticleModule, LanguageModule, TranslationModule, AuthModule, UiComponentModule],
  controllers: [AppController],
  providers: [AppService,
    {
      provide: APP_FILTER,
      useClass: MultilingoSystemErrorFilter,
    },
  ],
})
export class AppModule { }
