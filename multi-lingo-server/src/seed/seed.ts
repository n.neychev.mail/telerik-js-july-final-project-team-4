import { createConnection, Transaction } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { User } from '../database/entities/user.entity';

import { Group } from '../database/entities/group.entity';
import { Permission } from '../database/entities/permission.entity';
import { GroupName } from '../common/enum/group-name.enum';
import { Language } from '../database/entities/language.entity';

import { PermissionName } from '../common/enum/permission.enum';
import { UiComponent } from '../database/entities/ui-component.entity';
import { UiTranslation } from '../database/entities/ui-translation.entity';
import { UiComponentService } from '../modules/ui-components/ui-component.service';
import { LanguageService } from '../modules/language/language.service';
import { TranslationService } from '../modules/translation/translation.service';
import { Phrase } from '../database/entities/phrase.entity';
import { Translation } from '../database/entities/translation.entity';
import { Rate } from '../database/entities/rating.entity';

const main = async () => {

    const connection = await createConnection();
    const userRepo = connection.getRepository(User);
    const groupRepo = connection.getRepository(Group);
    const permissionRepo = connection.getRepository(Permission);
    const phraseRepo = connection.getRepository(Phrase);
    const translationRepo = connection.getRepository(Translation);
    const languageRepo = connection.getRepository(Language);
    const rateRepo = connection.getRepository(Rate);
    const uiComponentRepo = connection.getRepository(UiComponent);
    const uiTranslationRepo = connection.getRepository(UiTranslation);
    
    const uiComponentService = new UiComponentService(uiComponentRepo, uiTranslationRepo, languageRepo);

    // Creating groups
    let contributorGroup = new Group();
    contributorGroup.name = GroupName.contributor;
    contributorGroup = await groupRepo.save(contributorGroup);

    let editorGroup = new Group();
    editorGroup.name = GroupName.editor;
    editorGroup = await groupRepo.save(editorGroup);

    let adminGroup = new Group();
    adminGroup.name = GroupName.admin;
    adminGroup = await groupRepo.save(adminGroup);

    // Creating permissions
    // Article
    let readArticle = new Permission();
    readArticle.name = PermissionName.Read_Article;
    readArticle = await permissionRepo.save(readArticle);

    let updateArticles = new Permission();
    updateArticles.name = PermissionName.Update_Articles;
    updateArticles = await permissionRepo.save(updateArticles);

    let updateYourArticles = new Permission();
    updateYourArticles.name = PermissionName.Update_Your_Articles;
    updateYourArticles = await permissionRepo.save(updateYourArticles);

    let deleteArticle = new Permission();
    deleteArticle.name = PermissionName.Delete_Article;
    deleteArticle = await permissionRepo.save(deleteArticle);

    let createArticle = new Permission();
    createArticle.name = PermissionName.Create_Article;
    createArticle = await permissionRepo.save(createArticle);

    // Translation
    let updateTranslation = new Permission();
    updateTranslation.name = PermissionName.Update_Translation;
    updateTranslation = await permissionRepo.save(updateTranslation);

    let readTranslation = new Permission();
    readTranslation.name = PermissionName.Read_Translation;
    readTranslation = await permissionRepo.save(readTranslation);

    // User
    let createUser = new Permission();
    createUser.name = PermissionName.Create_User;
    createUser = await permissionRepo.save(createUser);

    let readUser = new Permission();
    readUser.name = PermissionName.Read_User;
    readUser = await permissionRepo.save(readUser);

    let updateUser = new Permission();
    updateUser.name = PermissionName.Update_User;
    updateUser = await permissionRepo.save(updateUser);

    let deleteUser = new Permission();
    deleteUser.name = PermissionName.Delete_User;
    deleteUser = await permissionRepo.save(deleteUser);

    let uiComponent = new Permission();
    uiComponent.name = PermissionName.Ui_Access;
    uiComponent = await permissionRepo.save(uiComponent);

    // Assigning permission to group
    // Contributor
    contributorGroup.permission = Promise.resolve([
        createArticle,
        updateYourArticles,
        deleteArticle,
    ]);

    await groupRepo.save(contributorGroup);

    // Editor
    editorGroup.permission = Promise.resolve([
        updateTranslation,
        readTranslation,
    ]);
    await groupRepo.save(editorGroup);

    // Admin
    adminGroup.permission = Promise.resolve([
        createArticle,
        updateArticles,
        deleteArticle,
        updateTranslation,
        readTranslation,
        updateUser,
        deleteUser,
        uiComponent,
        updateYourArticles,
    ]);
    await groupRepo.save(adminGroup);

    // Creating user
    const contributorUser = new User();
    contributorUser.username = 'Contributor';
    contributorUser.group = Promise.resolve([contributorGroup]);
    contributorUser.password = await bcrypt.hash('asd', 10);
    await userRepo.save(contributorUser);

    const editorUser = new User();
    editorUser.username = 'Editor';
    editorUser.group = Promise.resolve([editorGroup]);
    editorUser.password = await bcrypt.hash('asd', 10);
    await userRepo.save(editorUser);

    const adminUser = new User();
    adminUser.username = 'Admin';
    adminUser.group = Promise.resolve([adminGroup]);
    adminUser.password = await bcrypt.hash('asd', 10);
    await userRepo.save(adminUser);

    // Adding languages
    let english = new Language();
    english.name = 'English';
    english.symbol = 'en';
    english = await languageRepo.save(english);
    let bulgarian = new Language();
    bulgarian.name = 'Bulgarian';
    bulgarian.symbol = 'bg';
    bulgarian = await languageRepo.save(bulgarian);
    let russian = new Language();
    russian.name = 'Russian';
    russian.symbol = 'ru';
    russian = await languageRepo.save(russian);
    let italian = new Language();
    italian.name = 'Italian';
    italian.symbol = 'it';
    italian = await languageRepo.save(italian);
    // Create Ui
    const uiComponentStrings = ['Language', 'Original Text', 'Translation'];

    await Promise.all(uiComponentStrings.map((componentToCreate) => uiComponentService.createComponent(componentToCreate)));

    await connection.close();
    console.log(`Data seeded successfully`);
};

main()
    .catch(console.log);
