import { createConnection } from 'typeorm';
import { Translation } from '../database/entities/translation.entity';
import { User } from '../database/entities/user.entity';
import { Article } from '../database/entities/article.entity';
import { Group } from '../database/entities/group.entity';
import { Permission } from '../database/entities/permission.entity';
import { Language } from '../database/entities/language.entity';

const main = async () => {
    const connection = await createConnection();

    const translationRepo = connection.getRepository(Translation);
    const userRepo = connection.getRepository(User);
    const articleRepo = connection.getRepository(Article);
    const groupRepo = connection.getRepository(Group);
    const permissionRepo = connection.getRepository(Permission);
    const languagerepo = connection.getRepository(Language);

    await userRepo.delete({});
    await translationRepo.delete({});
    await articleRepo.delete({});
    await groupRepo.delete({});
    await permissionRepo.delete({});
    await languagerepo.delete({});

    await connection.close();

    console.log('Data removed from database!');

};
main()
.catch(console.log);
