import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';
import { Group } from './group.entity';

@Entity('permission')
export class Permission {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('nvarchar')
    public name: string;

    @ManyToMany(type => Group, group => group.permission)
    public group: Group[];
}
