import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';
import { Group } from './group.entity';
import { Phrase } from './phrase.entity';
import { User } from './user.entity';
import { ArticleVersion } from './article-version.entity';

@Entity('article')
export class Article {
    @PrimaryGeneratedColumn('uuid')
    public id: string;
    @Column('boolean', { default: false })
    public isDeleted: boolean;

    @OneToMany(type => ArticleVersion, version => version.article)
    public versions: Promise<ArticleVersion[]>;

    @ManyToOne(type => User, user => user.article)
    public user: Promise<User>;
}
