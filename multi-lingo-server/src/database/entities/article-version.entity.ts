import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany, Generated } from 'typeorm';
import { Group } from './group.entity';
import { Phrase } from './phrase.entity';
import { Article } from './article.entity';
import { Language } from './language.entity';

@Entity('article_version')
export class ArticleVersion {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'integer', default: 1 })
    public number: number;

    @Column({ type: 'boolean', default: true })
    public isCurrent: boolean;

    @ManyToOne(type => Article, article => article.versions, { eager: true, onDelete: 'CASCADE' })
    public article: Promise<Article>;

    @ManyToOne(type => Phrase, phrase => phrase.versionTitle, { eager: true })
    public title: Promise<Phrase>;

    @ManyToOne(type => Phrase, phrase => phrase.versionText, { eager: true })
    public text: Promise<Phrase>;

    @ManyToOne(type => Language, language => language.version)
    public language: Promise<Language>;
}
