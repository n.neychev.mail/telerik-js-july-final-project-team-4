import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';
import { Phrase } from './phrase.entity';
import { Language } from './language.entity';
import { Rate } from './rating.entity';
import { UiComponent } from './ui-component.entity';

@Entity('translation')
export class Translation {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('text')
    public text: string;

    @ManyToOne(type => Phrase, phrase => phrase.translations)
    public origin: Phrase;

    @ManyToOne(type => Language, language => language.translations, {eager: true})
    public language: Language;

    @OneToMany(type => Rate, rate => rate.translations)
    public rates: Promise<Rate[]>;
}
