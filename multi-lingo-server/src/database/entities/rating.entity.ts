import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';

import { Translation } from './translation.entity';
import { ArticleVersion } from './article-version.entity';

@Entity('rate')
export class Rate {
    @PrimaryGeneratedColumn('uuid')
    public id: string;
    @Column('integer')
    public amount: number;

    @ManyToOne(type => Translation, translation => translation.rates)
    public translations: Promise<Translation>;
}
