import { PrimaryGeneratedColumn, Column, Entity, OneToMany } from 'typeorm';
import { UiTranslation } from './ui-translation.entity';

@Entity('ui-component')
export class UiComponent {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('text')
    public text: string;

    @OneToMany(type => UiTranslation, translation => translation.uiComponent, {eager: true})
    public translations: UiTranslation[];

}
