import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';
import { Group } from './group.entity';
import { Article } from './article.entity';
import { Language } from './language.entity';

@Entity('user')
export class User {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('nvarchar')
    public username: string;

    @Column('nvarchar')
    public password: string;

    @Column({ type: 'boolean', default: false })
    public isDeleted: boolean;

    @Column( {type: 'boolean', default: false })
    public isBanned: boolean;

    @ManyToMany(type => Group, group => group.user)
    public group: Promise<Group[]>;

    @OneToMany(type => Article, article => article.user)
    public article: Promise<Article[]>;

    @ManyToOne(type => Language, language => language.users)
    public languagePreference: Promise<Language>;
}
