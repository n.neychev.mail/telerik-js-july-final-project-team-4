import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';

import { Translation } from './translation.entity';
import { ArticleVersion } from './article-version.entity';
import { UiComponent } from './ui-component.entity';

@Entity('phrase')
export class Phrase {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('text')
    public text: string;

    @OneToMany(type => ArticleVersion, article => article.text)
    public versionText: Promise<ArticleVersion[]>;

    @OneToMany(type => ArticleVersion, article => article.title)
    public versionTitle: Promise<ArticleVersion[]>;

    @OneToMany(type => Translation, translation => translation.origin, {eager: true})
    public translations: Translation[];
}
