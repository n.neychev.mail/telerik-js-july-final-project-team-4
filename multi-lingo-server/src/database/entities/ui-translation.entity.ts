import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';
import { Language } from './language.entity';
import { UiComponent } from './ui-component.entity';

@Entity('ui-translation')
export class UiTranslation {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('text')
    public text: string;

    @ManyToOne(type => UiComponent, component => component.translations)
    public uiComponent: UiComponent;

    @ManyToOne(type => Language, language => language.translations, {eager: true})
    public language: Language;

}
