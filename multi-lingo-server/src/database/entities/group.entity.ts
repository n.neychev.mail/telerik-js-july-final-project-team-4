import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';
import { User } from './user.entity';
import { Permission } from './permission.entity';

@Entity('group')
export class Group {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('nvarchar')
    public name: string;

    @ManyToMany(type => User, user => user.group)
    @JoinTable()
    public user: Promise<User[]>;

    @ManyToMany(type => Permission, perm => perm.group)
    @JoinTable()
    public permission: Promise<Permission[]>;
}
