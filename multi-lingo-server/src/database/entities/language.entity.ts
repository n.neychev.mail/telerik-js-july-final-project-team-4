import { PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Entity, OneToMany } from 'typeorm';
import { Translation } from './translation.entity';
import { ArticleVersion } from './article-version.entity';
import { User } from './user.entity';

@Entity('language')
export class Language {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('nvarchar')
    public name: string;

    @Column('nvarchar')
    public symbol: string;

    @OneToMany(type => Translation, translation => translation.language)
    public translations: Promise<Translation[]>;

    @OneToMany(type => ArticleVersion, version => version.language)
    public version: Promise<ArticleVersion[]>;

    @OneToMany(type => User, user => user.languagePreference)
    public users: Promise<User[]>;
}
