import { Injectable, CanActivate, ExecutionContext, Type } from '@nestjs/common';
import { UserService } from '../../modules/user/user.service';
import { PayloadDTO } from '../../core/auth/payload.dto';

export function PermissionGuard(permission: string): Type<CanActivate> {

  @Injectable()
  // tslint:disable-next-line: no-shadowed-variable
  class PermGuardImpl implements CanActivate {
    constructor(
      private readonly userService: UserService,
    ) { }

    canActivate(context: ExecutionContext): Promise<boolean> {

      const request = context.switchToHttp().getRequest();
      const user: PayloadDTO = request.user;

      return this.isActionPermitted(user.id);
    }

    private async isActionPermitted(userId: string): Promise<boolean> {
      const permissions = await this.userService.getUserPermissions(userId);
      return permissions.has(permission);
    }
  }

  return PermGuardImpl;
}
