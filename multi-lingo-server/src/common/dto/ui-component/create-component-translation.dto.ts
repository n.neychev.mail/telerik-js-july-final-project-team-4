import { Language } from 'src/database/entities/language.entity';
import { UiComponent } from '../../../database/entities/ui-component.entity';

export class CreateComponentTranslationDTO {
    public text: string;
    public language: Language;
    public uiComponent: UiComponent;
}
