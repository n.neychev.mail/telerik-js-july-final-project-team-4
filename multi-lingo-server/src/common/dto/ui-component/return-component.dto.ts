import { Publish } from '../../decorator/publish';
import { UiTranslation } from '../../../database/entities/ui-translation.entity';
import { ReturnUiTranslationDTO } from './return-ui-translation.dto';

export class ReturnComponentDTO {
    @Publish()
    public id: string;
    @Publish()
    public text: string;

    @Publish(ReturnUiTranslationDTO)
    public translations: UiTranslation;
}
