import { Publish } from "../../decorator/publish";

export class ReturnUiTranslationDTO {
    @Publish()
    public id: string;
    @Publish()
    public text: string;
}
