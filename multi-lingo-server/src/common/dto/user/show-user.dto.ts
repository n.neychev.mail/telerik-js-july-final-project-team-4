import { ArticleByUserDTO } from '../article/articles-by-user.dto';

export class ShowUserDTO {
    public id: string;
     public username: string;
     public articles: ArticleByUserDTO[];

}
