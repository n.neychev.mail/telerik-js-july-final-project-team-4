import { Article } from '../../../database/entities/article.entity';
import { ArticleByUserDTO } from '../article/articles-by-user.dto';

export class SingleUserDTO {
    public id: string;
    public username: string;
    public article: ArticleByUserDTO[];
}
