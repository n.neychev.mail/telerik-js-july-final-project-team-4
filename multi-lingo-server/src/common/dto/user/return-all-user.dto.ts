import { Publish } from '../../decorator/publish';
import { Group } from '../../../database/entities/group.entity';

export class ReturnAllUserDTO {
    @Publish()
    public id: string;
    @Publish()
    public username: string;
    @Publish()
    public group: Group;
}
