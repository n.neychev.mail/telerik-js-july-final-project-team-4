import { Publish } from '../../decorator/publish';
import { Article } from '../../../database/entities/article.entity';
import { ArticleByUserDTO } from '../article/articles-by-user.dto';
import { Language } from '../../../database/entities/language.entity';

export class ReturnSingleUserDTO {
    @Publish()
    public id: string;
    @Publish()
    public username: string;
    @Publish()
    public article: ArticleByUserDTO[];
    @Publish()
    public languagePreference: Language;
}
