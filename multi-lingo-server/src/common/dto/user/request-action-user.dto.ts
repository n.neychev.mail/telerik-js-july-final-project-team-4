import { IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserAction } from '../../enum/user-action';
import { GroupName } from '../../enum/group-name.enum';

export class UserActionDTO {
    @IsEnum(UserAction)
    @ApiModelProperty()
    public action: UserAction;
    @IsEnum(GroupName)
    @ApiModelProperty()
    public group: GroupName;
}
