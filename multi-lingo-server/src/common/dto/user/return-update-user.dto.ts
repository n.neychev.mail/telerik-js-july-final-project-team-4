import { Group } from "../../../database/entities/group.entity";

export class ReturnUpdatedUserDTO {
    public id: string;
    public username: string;
    public password: string;
}
