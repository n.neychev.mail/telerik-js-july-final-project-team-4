import { ApiModelProperty } from '@nestjs/swagger';
import { Language } from '../../../database/entities/language.entity';

export class LogInUserDTO {
  @ApiModelProperty()
  public username: string;

  @ApiModelProperty()
  public password: string;
}
