import { ApiModelProperty } from '@nestjs/swagger';
import { LanguageDTO } from '../language/language.dto';

export class UpdateUserDTO {
  @ApiModelProperty()
  public username: string;

  @ApiModelProperty()
  public password: string;

  @ApiModelProperty()
  public languagePreference: LanguageDTO;
}
