import { Language } from 'src/database/entities/language.entity';
import { Phrase } from 'src/database/entities/phrase.entity';

export class CreateVersionDTO {
    public language: Language;
    public text: Phrase;
    public title: Phrase;

}
