import { Article } from '../../../database/entities/article.entity';
import { PhraseDTO } from '../phase/phrase.dto';

export class ReturnVersionDTO {
    public id: string;
    public number: number;
    public isCurrent: boolean;
    public article: Article;
    public title: PhraseDTO;
    public text: PhraseDTO;
}
