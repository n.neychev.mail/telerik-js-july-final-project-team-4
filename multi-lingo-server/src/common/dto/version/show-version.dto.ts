import { Phrase } from '../../../database/entities/phrase.entity';
import { Publish } from '../../decorator/publish';

export class ShowVersionDTO {
    @Publish()
    public id: string;
    public title: Promise<Phrase>;
    @Publish()
    public text: Promise<Phrase>;
    public isCurrent: boolean;
}
