import { Article } from '../../../database/entities/article.entity';
import { Phrase } from '../../../database/entities/phrase.entity';

export class ArticleVersionDTO {
    public id: string;
    public number: number;
    public article: Promise<Article>;
    public title: Promise<Phrase>;
    public text: Promise<Phrase>;
}
