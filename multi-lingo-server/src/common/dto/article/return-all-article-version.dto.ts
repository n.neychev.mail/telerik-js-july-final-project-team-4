import { Publish } from '../../decorator/publish';
import { Phrase } from '../../../database/entities/phrase.entity';
import { Article } from '../../../database/entities/article.entity';

export class ReturnAllArticleVersionDTO {
    @Publish()
    public id: string;
    @Publish()
    public number: number;
    @Publish()
    public isCurrent: boolean;
    @Publish()
    public article: Article;
    @Publish()
    public title: Phrase;
    @Publish()
    public text: Phrase;

}
