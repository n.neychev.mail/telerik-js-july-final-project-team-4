import { Publish } from '../../decorator/publish';
import { ShowVersionDTO } from '../version/show-version.dto';

export class ArticleByUserDTO {
    @Publish()
    public id: string;
    @Publish()
    public version: ShowVersionDTO;
}
