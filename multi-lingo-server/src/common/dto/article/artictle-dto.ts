import { User } from 'src/database/entities/user.entity';

export class ArticleDTO {
    public versions: string;
    public user: User;
}
