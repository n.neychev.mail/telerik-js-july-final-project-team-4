export class CreateArticleDTO {
    public title: string;
    public text: string;
}
