import { Phrase } from 'src/database/entities/phrase.entity';
import { Language } from 'src/database/entities/language.entity';
import { Rate } from 'src/database/entities/rating.entity';

export class CreateTranslationDTO {
    public text: string;
    public origin: Phrase;
    public language: Language;
    public rates: Promise<Rate[]>;
}
