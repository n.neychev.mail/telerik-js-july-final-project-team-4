import { Publish } from '../../decorator/publish';

export class ReturnTranslationDTO {
    @Publish()
    public text: string;
}
