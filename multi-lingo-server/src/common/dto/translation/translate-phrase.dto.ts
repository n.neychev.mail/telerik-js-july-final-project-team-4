export class TranslatePhraseDTO {
    public phraseId: string;
    public languageId: string;
}
