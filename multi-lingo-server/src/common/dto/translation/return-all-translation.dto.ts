import { Publish } from '../../decorator/publish';
import { Expose } from 'class-transformer';
import { Rate } from '../../../database/entities/rating.entity';
import { Language } from '../../../database/entities/language.entity';
import { Phrase } from '../../../database/entities/phrase.entity';

export class ReturnAllTranslationDTO {
    @Publish()
    public id: string;
    @Publish()
    public text: string;
    @Publish()
    public language: Language;
    @Publish()
    public rates: Promise<Rate[]>;
    @Publish()
    public origin: Phrase;
}
