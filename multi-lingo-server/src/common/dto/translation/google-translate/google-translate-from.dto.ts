import { GoogleTranslateFromLanguageDTO } from './google-translate-from-language.dto';
import { GoogleTranslateFromTextDTO } from './google-translate-from-text.dto';

export class GoogleTranslateFromDTO {
    public language: GoogleTranslateFromLanguageDTO;
    public text: GoogleTranslateFromTextDTO;
}
