export class GoogleTranslateFromLanguageDTO {
    public didYouMean: boolean;
    public iso: string;
}
