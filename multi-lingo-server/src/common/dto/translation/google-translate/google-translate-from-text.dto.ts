export class GoogleTranslateFromTextDTO {
    public autoCorrected: boolean;
    public value: string;
    public didYouMean: boolean;
}
