import { GoogleTranslateFromDTO } from './google-translate-from.dto';

export class GoogleTranslateDTO {
    public text: string;
    public pronunciation: string;
    public from: GoogleTranslateFromDTO;
    public raw: string;
}
