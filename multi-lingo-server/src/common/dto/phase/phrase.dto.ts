import { Translation } from '../../../database/entities/translation.entity';

export class PhraseDTO {
    public id: string;
    public text: string;
    public translations: Translation;
}
