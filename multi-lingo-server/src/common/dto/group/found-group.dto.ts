import { User } from '../../../database/entities/user.entity';
import { ReturnSingleUserDTO } from '../user/return-single-user.dto';

export class FoundGroupDTO {
    public id: string;
    public name: string;
    public user: Promise<ReturnSingleUserDTO[]>;
}
