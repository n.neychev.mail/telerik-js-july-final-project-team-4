export class LanguageDTO {
    public id: string;
    public name: string;
    public symbol: string;
}
