export class CreateLanguageDTO {
    public name: string;
    public symbol: string;
}
