export enum GroupName {
    contributor = 'Contributor',
    editor = 'Editor',
    admin = 'Admin',
}
