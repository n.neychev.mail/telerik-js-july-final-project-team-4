export enum PermissionName {
  Read_Article = 'Read Article',
  Update_Articles = 'Update Articles',
  Update_Your_Articles = 'Update Your Articles',
  Delete_Article = 'Delete Article',
  Create_Article = 'Create Article',
  Update_Translation = 'Update Translation',
  Read_Translation = 'Read Translation',
  Create_User = 'Create User',
  Read_User = 'Read User',
  Update_User = 'Update User',
  Delete_User = 'Delete User',
  Ui_Access = 'Ui Access',
}
