import { createParamDecorator } from '@nestjs/common';
import { PayloadDTO } from '../../core/auth/payload.dto';
// tslint:disable-next-line: variable-name
export const RequestUser = createParamDecorator((_, req) => req.user);
