import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { TranslationSystemError } from '../exceptions/multilingo-system-error';

@Catch(TranslationSystemError)
export class MultilingoSystemErrorFilter implements ExceptionFilter {
  public catch(exception: TranslationSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
