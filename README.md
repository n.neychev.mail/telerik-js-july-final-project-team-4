Multi-lingo
Multilingo is an aplication for  translation of articles .It suprots autro translation by all the suported languages  of Multi-lingo.translations can be rated and all have an average rating.All translations can be edited by user with the role Editor ,Editor roels are asigned by the admin

Running the server
Before running the project you should create two files - .env and ormconfig.ts 
Before running the project you should create two files - .env and ormconfig.ts 

In the server root folder create .env file

For mySQL database, copy the code below and paste it in the .env file:


PORT=3000
DB_TYPE=mysql
DB_HOST=localhost
DB_PORT=3306
DB_USERNAME=yourUserName
DB_PASSWORD=yourPasswordForDataBase
DB_DATABASE_NAME=yourDatabaseName
JWT_SECRET=yourSecretKey
JWT_EXPIRE_TIME=3600
GOOGLE_APPLICATION_CREDENTIALS=src/config/google-service-account.json

In the server root folder create ormconfig.ts file

For mySQL database, copy the code below and paste it in the ormconfig.ts file:


// Check typeORM documentation for more information.
const config: ConnectionOptions = {
type: 'mysql',
host: 'localhost',
port: 3306,
username: 'yourUserName',
password: 'yourPasswordForDataBase',
database: 'yourDatabaseName',
entities: ['src/database/entities/**/*.ts'],



export = config;
# Install all dependencies
$ npm install
# Run database seed
$ npm run seed
# Start the server in development mode
$ npm run start

# Start the server in watch mode
$ npm run start:dev
# For tests run the following commands
$ npm run test

the database seed contains
# User profile Admin
username: Admin,
password: asd

# User profile Editor
username: Contributor,
password: asd

# User profile Contributor
username: Editor,
password: asd