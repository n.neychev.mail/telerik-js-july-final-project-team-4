import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatInputModule,
  MatGridListModule,
  MatListModule,
  MatDividerModule,
  MatTabsModule,
  MatSidenavModule,
  MatIconModule,
  MatMenuModule,
  MatTooltipModule
} from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCheckboxModule } from '@angular/material/checkbox';

const modules = [
  CommonModule,
  MatButtonModule,
  MatCardModule,
  FormsModule,
  ReactiveFormsModule,
  MatInputModule,
  MatGridListModule,
  MatToolbarModule,
  MatCheckboxModule,
  MatListModule,
  MatDividerModule,
  MatTabsModule,
  MatSidenavModule,
  MatIconModule,
  MatMenuModule,
  MatTooltipModule,

];
@NgModule({
  declarations: [],
  imports: [
    ...modules
  ],
  exports: [
    ...modules
  ]
})
export class SharedModule { }
