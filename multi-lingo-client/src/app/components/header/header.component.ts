import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { Router } from '@angular/router';

import { AuthService } from '../../core/services/auth.service';
import { RegisteredUserDTO } from '../../common/dto/user/register-user.dto';
import { NotificationService } from '../../core/services/notification.service';
import { LanguageService } from '../../core/services/language.service';
import { LanguageDTO } from '../../common/dto/language/language.dto';
import { PermissionService } from '../../core/services/permission.service';
import { PermissionName } from '../../common/enum/permission.enum';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  private languageDataSubscription: Subscription;
  public loggedUserData: RegisteredUserDTO;
  public languageData: LanguageDTO[];

  public get translationPermission(): boolean {
    return this.permissionService.isUserPermitted(PermissionName.Read_Translation);
  };
  public get adminPermission(): boolean {
    return this.permissionService.isUserPermitted(PermissionName.Update_User);
  };
constructor(
    private readonly authService: AuthService,
    private readonly notification: NotificationService,
    private readonly languageService: LanguageService,
    private readonly router: Router,
    private readonly permissionService: PermissionService,


  ) { }

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: RegisteredUserDTO) => this.loggedUserData = data);

    this.languageDataSubscription = this.languageService.languageData$.subscribe(
      (data: LanguageDTO[]) => this.languageData = data);
  }

  public ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
    this.languageDataSubscription.unsubscribe();
  }

  logout() {
    this.authService.logout();
    this.notification.success('Successfully logged out!');
    this.router.navigate(['home']);
  }
  goToUserProfile() {
    this.router.navigate([`users/user-profile/${this.loggedUserData.id}`]);
  }


}
