import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-create-new-version',
  templateUrl: './create-new-version.component.html',
  styleUrls: ['./create-new-version.component.css']
})
export class CreateNewVersionComponent implements OnInit {
  public createVersionForm: FormGroup;
  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
  ) { }
  @Input() previousCurrentVersion;
  @Output() createEmit = new EventEmitter<any>();
  ngOnInit() {
    this.createVersionForm = this.formBuilder.group({
      title: [''],
      text: [
        '',
        [Validators.required, Validators.minLength(10)]
      ],
    });

  }
  public onClickCreate(value) {
    const idUser: string = this.activatedRoute.snapshot.params.id;
    const body = {
      id: idUser,
      data: value
    };
    this.createEmit.emit(body);
  }

}
