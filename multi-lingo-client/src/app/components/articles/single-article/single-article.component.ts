import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { SingleArticleDTO } from '../../../common/dto/article/single-article.dto';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';
import { ArticleService } from '../../../core/services/article.service';
import { SingleVersionDTO } from '../../../common/version/dto/single-version.dto';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit, OnDestroy {
  private getArticleInfoSubscription: Subscription;
  public article: SingleArticleDTO;
  public versions: SingleVersionDTO[];
  private createVersion = false;
  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly articleService: ArticleService,
    private readonly notificator: NotificationService,

  ) { }

  ngOnInit() {
    const id: string = this.activatedRoute.snapshot.params.id;
    this.getArticleInfoSubscription = this.articleService.getArticleById(id).subscribe((data) => {
      this.article = data;
      this.versions = this.article.versions;
      return this.versions;
    });
    this.createVersion = false;
  }
  ngOnDestroy(): void {
    this.getArticleInfoSubscription.unsubscribe();
  }
  updateCurrent(versionToBeCurrent) {
    const body = {};
    this.articleService.changeCurrentArticle(versionToBeCurrent, body).subscribe(
      (data) => {
      this.notificator.success(`version updated!`);
      this.article = data;
      return this.article;
    },
    (error) => {
      this.notificator.error(error.error.error);
    },
    );
  }
  public showCreateVersion(): void {
    this.createVersion = true;
  }
  public addNewVersion(body): void {
    this.articleService.createNewVersion(body.id, body.data).subscribe(
      (data) => {
        this.notificator.success(`new version created!`);
        const versionToAdd = (data.__versions__.filter(x => x.isCurrent === true));


        this.versions.push(versionToAdd[0]);
      },
      (error) => {
        this.notificator.error(error.error.error);
      },
    );
    this.createVersion = false;
  }

}
