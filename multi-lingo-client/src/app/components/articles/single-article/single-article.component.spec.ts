import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { SingleArticleComponent } from './single-article.component';
import { SharedModule } from '../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AllVersionsComponent } from '../all-versions/all-versions.component';
import { CreateNewVersionComponent } from '../create-new-version/create-new-version.component';
import { ArticleService } from '../../../core/services/article.service';
import { NotificationService } from '../../../core/services/notification.service';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { of, throwError } from 'rxjs';

describe('Single article Component', () => {
  const articleService = {
    getArticleById() {

    },
    changeCurrentArticle() { },
    createNewVersion() { },
  };
  const activatedRoute = {
  };
  const notificator = {
    success() { },
    error() { },
  };

  const spyActiveRoute = jest.spyOn(ActivatedRoute, 'snapshot').mockImplementation(() => {id: 1});
  const routeSnapshot = ({ data: of({ id: '1' }) } as any) as ActivatedRoute;

  let fixture: ComponentFixture<SingleArticleComponent>;
  let component: SingleArticleComponent;
  beforeEach(async(() => {
    jest.clearAllMocks();
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        BrowserAnimationsModule,
      ],
      declarations: [AllVersionsComponent, CreateNewVersionComponent, SingleArticleComponent],
      providers: [ActivatedRoute, ArticleService, NotificationService],
    })
      .overrideProvider(ArticleService, { useValue: articleService })
      .overrideProvider(NotificationService, { useValue: notificator })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(SingleArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();

  });

  it('should update current version', () => {
    // Arrange
    const successSpy = jest.spyOn(articleService, 'changeCurrentArticle');
    // Act
    component.updateCurrent('version');
    // Assert
    fixture.detectChanges();
    expect(articleService.changeCurrentArticle).toHaveBeenCalledTimes(1);
  });

  it('should notify after updating current version', () => {
    // Arrange
    const successSpy = jest.spyOn(notificator, 'success');
    const loginSpy = jest.spyOn(articleService, 'changeCurrentArticle').mockImplementation(() => of(true));
    // Act
    component.updateCurrent('version');
    // Assert
    fixture.detectChanges();
    expect(notificator.success).toHaveBeenCalledWith(`version updated!`);
  });

  it('should notify after if update has failed', () => {
    // Arrange
    const successSpy = jest.spyOn(notificator, 'error');
    const loginSpy = jest.spyOn(articleService, 'changeCurrentArticle').mockImplementation(() => throwError(new Error()));
    // Act
    component.updateCurrent('version');
    // Assert
    fixture.detectChanges();
    expect(notificator.error).toHaveBeenCalledTimes(1);
  });

  it('should craete new version', () => {
    // Arrange
    const successSpy = jest.spyOn(articleService, 'createNewVersion');
    // Act
    component.addNewVersion( {id: '1', data: {}} );
    // Assert
    fixture.detectChanges();
    expect(articleService.createNewVersion).toHaveBeenCalledTimes(1);
  });

  it('should notify after creating new version', () => {
    // Arrange
    const successSpy = jest.spyOn(notificator, 'success');
    const loginSpy = jest.spyOn(articleService, 'createNewVersion').mockImplementation(() => of(true));
    // Act
    component.updateCurrent('version');
    // Assert
    fixture.detectChanges();
    expect(notificator.success).toHaveBeenCalledWith(`new version created!`);
  });

  it('should notify after if creating a new version has failed', () => {
    // Arrange
    const successSpy = jest.spyOn(notificator, 'error');
    const loginSpy = jest.spyOn(articleService, 'changeCurrentArticle').mockImplementation(() => throwError(new Error()));
    // Act
    component.updateCurrent('version');
    // Assert
    fixture.detectChanges();
    expect(notificator.error).toHaveBeenCalledTimes(1);
  });

});
