import { Component, OnInit} from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ArticleService } from '../../../core/services/article.service';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {
  public subscription: Subscription;
  public createArticleForm: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly articleService: ArticleService,
    private readonly notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.createArticleForm = this.formBuilder.group({
      title: [''],
      text: [
        '',
        [Validators.required, Validators.minLength(10), Validators.maxLength(50)]
      ],
    });
  }

  onClickCreate(value) {
    const langueds = 'en';
    const bodys = {
      text: value.text,
      title: value.title,
      language: langueds
    };
    this.articleService.createArticle(bodys).subscribe(
      () => this.notificationService.success('article created'),
      () => this.notificationService.error('Invalid article data!'));
  }
}
