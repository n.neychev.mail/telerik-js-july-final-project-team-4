import { NgModule } from '@angular/core';
import { SingleArticleComponent } from './single-article/single-article.component';
import { ArticlesRoutingModule } from './article-roouting.module';
import { ArticleService } from '../../core/services/article.service';
import { AllVersionsComponent } from './all-versions/all-versions.component';
import { SharedModule } from '../../shared/shared.module';
import { CreateArticleComponent } from './create-article/create-article.component';
import { CreateNewVersionComponent } from './create-new-version/create-new-version.component';

@NgModule({
    imports: [ArticlesRoutingModule, SharedModule],
    declarations: [
        SingleArticleComponent
        , AllVersionsComponent
        , CreateArticleComponent
        , CreateNewVersionComponent,
    ],
    providers: [ArticleService],
    exports: [],
})
export class ArticlesModule {

}
