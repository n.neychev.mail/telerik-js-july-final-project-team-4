import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ArticleService } from '../../../core/services/article.service';

@Component({
  selector: 'app-all-versions',
  templateUrl: './all-versions.component.html',
  styleUrls: ['./all-versions.component.css']
})
export class AllVersionsComponent implements OnInit {

  constructor(
    private readonly articleservice: ArticleService, ) {

  }
  @Input() public version;
  @Output() createEmit = new EventEmitter<any>();
  ngOnInit() {
  }
  makeVersionCurrent(): void {
      this.createEmit.emit(this.version.id);
  }

}
