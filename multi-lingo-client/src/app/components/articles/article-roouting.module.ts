import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SingleArticleComponent } from './single-article/single-article.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import { AuthGuard } from '../../common/guard/auth.guard';
const routes: Routes = [
{
    path: 'create', component: CreateArticleComponent, canActivate: [AuthGuard],

},
{
        path: ':id', component: SingleArticleComponent, canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ArticlesRoutingModule { }
