import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AllTranslationsComponent } from './all-translations/all-translations.component';
import { SingleTranslationComponent } from './single-translation/single-translation.component';
import { PermissionGuard } from '../../common/guard/permission.guard';
import { PermissionName } from '../../common/enum/permission.enum';

const routes: Routes = [
    {path: '', component: AllTranslationsComponent,
    canActivate:[PermissionGuard],
    data: { permission: PermissionName.Read_Translation }},
    {path: ':id', component: SingleTranslationComponent,
    canActivate:[PermissionGuard],
    data: { permission: PermissionName.Update_Translation }},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TranslationRoutingModule { }
