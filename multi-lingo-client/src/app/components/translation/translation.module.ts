import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { TranslationService } from '../../core/services/translation.service';
import { AllTranslationsComponent } from './all-translations/all-translations.component';
import { TranslationRoutingModule } from './translation-routing.module';
import { SingleTranslationComponent } from './single-translation/single-translation.component';
import { UiComponentService } from '../../core/services/ui-component.service';

@NgModule({
    imports: [SharedModule, TranslationRoutingModule],
    declarations: [AllTranslationsComponent, SingleTranslationComponent],
    providers: [TranslationService, UiComponentService],
    exports: [],
})
export class TranslationModule {
}
