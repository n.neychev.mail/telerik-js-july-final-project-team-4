import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TranslationDTO } from '../../../common/dto/translation/translation.dto';
import { Subscription } from 'rxjs';
import { TranslationService } from '../../../core/services/translation.service';
import { Router } from '@angular/router';
import { UpdateTranslationDTO } from '../../../common/dto/translation/update-translation.dto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../../core/services/notification.service';
import { changedInput } from '../../../common/validator/changed.validator';

@Component({
  selector: 'app-single-translation',
  templateUrl: './single-translation.component.html',
  styleUrls: ['./single-translation.component.css']
})
export class SingleTranslationComponent implements OnInit {
  private subscription: Subscription;
  public translation: TranslationDTO;
  public updateTranslationForm: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    public readonly translationService: TranslationService,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
  ) { }
  ngOnInit(): void {
    this.translation = this.translationService.translation;
    this.updateTranslationForm = this.formBuilder.group({
      text: [
        this.translation.text,
        [Validators.required, Validators.minLength(10), Validators.maxLength(50), changedInput(this.translation.text)]
      ],
    });

  }

  public updateTranslation(body: UpdateTranslationDTO) {
    this.translationService.updateTranslation(this.translation.id, body)
  }

  onSubmit(formValue: UpdateTranslationDTO) {

    this.translationService.updateTranslation(this.translation.id, formValue).subscribe(
      () => this.notificationService.success('new translation created'),
      () => this.notificationService.error('Invalid article data!'));
  }

}
