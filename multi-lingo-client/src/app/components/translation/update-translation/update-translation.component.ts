import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TranslationDTO } from '../../../common/dto/translation/translation.dto';


@Component({
  selector: 'app-update-translation',
  templateUrl: './update-translation.component.html',
  styleUrls: ['./update-translation.component.css']
})
export class UpdateTranslationComponent implements OnInit, OnDestroy {
  constructor(
  ) { }

  @Input() public translation: TranslationDTO;
  ngOnInit(): void {

  }
  ngOnDestroy(): void {
  }

}
