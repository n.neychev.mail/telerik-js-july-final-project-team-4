import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TranslationDTO } from '../../../common/dto/translation/translation.dto';
import { Subscription } from 'rxjs';
import { TranslationService } from '../../../core/services/translation.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../../core/services/notification.service';
import { UiComponentService } from '../../../core/services/ui-component.service';
import { UiComponentDTO } from '../../../common/dto/ui-component/ui-component.dto';

@Component({
  selector: 'app-all-translation',
  templateUrl: './all-translations.component.html',
  styleUrls: ['./all-translations.component.css']
})
export class AllTranslationsComponent implements OnInit, OnDestroy {
  private translationSubscription: Subscription;
  private uiSubscription: Subscription;
  private uiComponentsToGet = [
    'Language',
    'Translation',
    'Original Text'
  ];
  private uiComponents = new Map<string, UiComponentDTO>();

  constructor(
    public readonly translationService: TranslationService,
    public readonly uiComponentService: UiComponentService,
    private readonly router: Router,
    private readonly notificator: NotificationService,
  ) { }
  public translations: TranslationDTO[];
  ngOnInit(): void {

    this.translationSubscription = this.translationService.getAllTranslations().subscribe(
      (data) => { this.translations = data; },
      (error) => {
        this.notificator.error('not allowed');
      },
    );

    this.uiSubscription = this.uiComponentService.getUiComponents(this.uiComponentsToGet).subscribe(
      (data) => {
        this.uiComponents = data.reduce((accumulator, component): Map<string, UiComponentDTO> => {
          accumulator.set(component.text, component);
          return accumulator;
        }, new Map<string, UiComponentDTO>());
      },
      (error) => {
        this.notificator.error('not allowed');
      },
    );
  }
  ngOnDestroy(): void {
    this.translationSubscription.unsubscribe();
    this.uiSubscription.unsubscribe();
  }
  public onTranslationClick(translation: TranslationDTO): void {
    this.translationService.translation = translation;
    this.router.navigate([`translation/${this.translationService.translation.id}`]);
  }

}
