import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TranslationService } from '../../core/services/translation.service';
import { NotificationService } from '../../core/services/notification.service';
import { PermissionName } from '../../common/enum/permission.enum';
import { PermissionService } from '../../core/services/permission.service';
import { ArticleService } from '../../core/services/article.service';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.css']
})
export class SingleComponent implements OnInit {
 

  private subscription: Subscription;
  public constructor(
    private readonly router: Router,
    private readonly translationService: TranslationService,
    private readonly notificator: NotificationService,
    private readonly permissionService: PermissionService,
    private readonly articleService: ArticleService ) {

  }
  public get adminPermission(): boolean {
    return this.permissionService.isUserPermitted(PermissionName.Update_User);
  };

  @Input()public version;
@Output() deleteArticleClick: EventEmitter<any> = new EventEmitter<any>();
  ngOnInit() {

  }
  public rate(values: string) {
    const idText = (this.version.text.translations.id);
    const idTitle = (this.version.title.translations.id);
    const body = {
      value: values,
      textId: idText,
      titleId: idTitle
    };
    this.translationService.rateTranslations(body).subscribe(
      () => {
        this.notificator.success(`translate rated!`);
        this.router.navigate(['home']);
        location.reload();
      },
      (error) => {
        this.notificator.error(error.error.error);
      },
    );
  }
  public updateArticle(): void {
    this.articleService.articleToUpdate = this.version.article;
    this.router.navigate([`articles/${this.version.article.id}`, ]);
  }
  public deleteArticle(): void {

    const id: string = (this.version.article.id);
    this.articleService.deleteArticle(id).subscribe(data => {
      return this.version.article = data;
    });
    this.deleteArticleClick.emit(this.version);
  }

}
