import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { ArticleDTO } from '../../common/dto/article/article.dto';
import { ArticleService } from '../../core/services/article.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  public allArticles: ArticleDTO[];


  constructor(
    private readonly articleService: ArticleService,
  ) { }
  ngOnInit(): void {

    this.subscription = this.articleService.getAllArticles().subscribe(data => {
      this.allArticles = data;
      return this.allArticles;
    });
  }
  public delete(articleToDelete): void {
    this.allArticles = this.allArticles.filter(x => x.id !== articleToDelete.id);
  }
  ngOnDestroy(): void {
    if (this.subscription){
      this.subscription.unsubscribe()};
  }
}
