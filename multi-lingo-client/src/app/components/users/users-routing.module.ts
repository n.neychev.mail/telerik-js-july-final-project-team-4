
import { RegisterComponent } from './register/register.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

import { UsersComponent } from './users.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AuthGuard } from '../../common/guard/auth.guard';
import { UserResolver } from '../../common/resolver/user.resolver';
import { AdminActionDTO } from '../../common/dto/admin/admin-action.dto';
import { AdminPartComponent } from './admin/admin-part/admin-part.component';
import { PermissionGuard } from '../../common/guard/permission.guard';
import { PermissionName } from '../../common/enum/permission.enum';


const userRoutes: Routes = [

  {
    path: '',
    component: AdminPartComponent,
    pathMatch: 'full',
    canActivate: [PermissionGuard],
    data: { permission: PermissionName.Update_User }
  },

  { path: 'register', component: RegisterComponent },

  { path: 'login', component: LoginComponent },
  {
    path: 'user-profile/:id', component: UserProfileComponent,
    canActivate: [AuthGuard],
    resolve: {
      user: UserResolver,
    }
  }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes)
  ],
  providers: [
    UserResolver,
  ]
})
export class UsersRoutingModule { }
