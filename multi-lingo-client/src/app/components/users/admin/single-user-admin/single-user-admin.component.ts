import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../../../core/services/user.service';
import { Subscription } from 'rxjs';
import { ReturnUserAdminPanelDTO } from '../../../../common/dto/user/return-user-admin-panel.dto';
import { PermissionService } from '../../../../core/services/permission.service';
import { PermissionName } from '../../../../common/enum/permission.enum';



@Component({
  selector: 'app-single-user-admin',
  templateUrl: './single-user-admin.component.html',
  styleUrls: ['./single-user-admin.component.css']
})
export class SingleUserAdminComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private roleSubscription: Subscription;
  public userGroups;
  public isEditor: boolean;
  public isAdmin: boolean;

  public isContributor: boolean;

  private privateUser: ReturnUserAdminPanelDTO;
  constructor(
    private readonly userService: UserService,
    private readonly permissionService: PermissionService,
  ) { }
   // tslint:disable-next-line: variable-name
  @Output() deleteUserClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeUserRoleClick: EventEmitter<any> = new EventEmitter<any>();

  @Input() set user(user: ReturnUserAdminPanelDTO) {
    this.privateUser = user;
    this.userGroups = user.group;
    this.isAdmin = this.userGroups.some(x => x.name === 'Admin');
    this.isEditor = this.userGroups.some(x => x.name === 'Editor');
    this.isContributor = this.userGroups.some(x => x.name === 'Contributor');
  }

  get user(): ReturnUserAdminPanelDTO {
    return this.privateUser;
  }

  ngOnInit() {

  }

  ngOnDestroy() {
  }
  public deleteUser(): void {
    const id: string = (this.user.id);
    this.deleteUserClick.emit(id);
  }
  public addRole(value): void {
    const body = {
      action: 'Add',
      group: value,
      id: this.user.id
    };
    this.changeUserRoleClick.emit(body);
  }
  public removeRole(value): void {
    const body = {
      action: 'Remove',
      group: value,
      id: this.user.id
    };
    this.changeUserRoleClick.emit(body);
  }
  public get makeAdminPermission(): boolean {
    return this.permissionService.isUserPermitted(PermissionName.Update_User);
  }

}
