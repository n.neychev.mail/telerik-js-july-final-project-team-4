import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from '../../../../core/services/user.service';
import { UserDTO } from '../../../../common/dto/user/user.dto';
import { NotificationService } from '../../../../core/services/notification.service';

@Component({
  selector: 'app-admin-part',
  templateUrl: './admin-part.component.html',
  styleUrls: ['./admin-part.component.css']
})
export class AdminPartComponent implements OnInit, OnDestroy {
  public allUsers: UserDTO[];
  private subscription: Subscription;
  private subscriptionUser: Subscription;

  constructor(
    private readonly userService: UserService,
    private readonly notification: NotificationService,
    private readonly changeDetection: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.subscription = this.userService.getAllUsers().subscribe(data => {
      this.allUsers = data;
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  public delete(id): void {
    this.subscription = this.userService.deleteUser(id).subscribe(data => {
      this.allUsers = this.allUsers.filter(x => x.id !== data.id);
    })
    
  }
  public changeRole(body) {
    const action = {
      group: body.group,
      action: body.action
    };
    this.subscriptionUser = this.userService.changeUserRole(body.id, action).subscribe(userData => {

      const allUsers = this.allUsers.map(user => {
        if (user.id === userData.id) {
          const updatedUser = Object.assign({}, user, { group: userData.group });
          return updatedUser;
        }
        return user;
      });

      this.allUsers = [...allUsers];
      this.changeDetection.detectChanges();
      this.notification.success(`User group updated`);
    }, (error => {
      this.notification.error(`User could not be added to group`);
    }));
  }
}
