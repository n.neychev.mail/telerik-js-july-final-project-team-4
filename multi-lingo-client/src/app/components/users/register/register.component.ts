import { NotificationService } from '../../../core/services/notification.service';
import { AuthService } from '../../../core/services/auth.service';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;

  constructor(
              private readonly formBuilder: FormBuilder,
              private readonly authService: AuthService,
              private readonly notificator: NotificationService,
              private readonly router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(4),  Validators.maxLength(15)]],
      password: ['', [Validators.required, Validators.minLength(4),  Validators.maxLength(15)]]
    });
  }

  register() {
    this.authService.register(this.registerForm.value).subscribe(
      () => {
        this.notificator.success('User successfully registered!');
        this.router.navigate(['users/login']);
      },
      (error) => {
        this.notificator.warning(error.error.error);
      }
    );
  }


}
