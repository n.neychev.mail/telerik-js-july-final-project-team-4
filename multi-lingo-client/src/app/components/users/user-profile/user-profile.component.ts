import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from '../../../core/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

import { ShowUserDTO } from '../../../common/dto/user/show-user.dto';
import { LanguageService } from '../../../core/services/language.service';
import { LanguageDTO } from '../../../common/dto/language/language.dto';
import { PermissionService } from '../../../core/services/permission.service';
import { PermissionName } from '../../../common/enum/permission.enum';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private languageDataSubscription: Subscription;
  private userLanguagePreferenceSubscription: Subscription;
  public user: ShowUserDTO = new ShowUserDTO();
  public articles;
  public languageData: LanguageDTO[];
  private showArticles: boolean;

  constructor(
    private readonly userService: UserService,
    private readonly languageService: LanguageService,
    private readonly permissionService: PermissionService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly notificator: NotificationService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    const id: string = this.activatedRoute.snapshot.params.id;
    this.subscription = this.userService.getUsersById(id).subscribe(data => {
      this.user = data,
        this.articles = this.user.article;
    });
    this.languageDataSubscription = this.languageService.languageData$.subscribe(
      (data: LanguageDTO[]) => this.languageData = data);
    this.showArticles = false;

  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.languageDataSubscription.unsubscribe();
    if (this.userLanguagePreferenceSubscription) {
      this.userLanguagePreferenceSubscription.unsubscribe();
    }
  }
  public delete(articleToDelete): void {
    this.articles = this.articles.filter(x => x.id !== articleToDelete.id);
  }
  public goToCreateArticle(): void {
    this.router.navigate([`articles/create`]);
  }
  public changeLanguagePref(value: LanguageDTO): void {
    const newLanguage = this.languageService.updateUserLanguageRequest(this.user.id, value);
    this.user.languagePreference = newLanguage; // Keeping the user object in sync with changes
    this.notificator.success('Your language was changed');
  }

  public get userLanguagePreference(): string {
    return this.languageService.languagePreference ? this.languageService.languagePreference.name : 'No preference given';
  }
}
