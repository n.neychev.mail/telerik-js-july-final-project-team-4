
import { UsersRoutingModule } from './users-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { SharedModule } from '../../shared/shared.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SingleArticleUserComponent } from './single-article-user/single-article-user.component';
import { ArticleService } from '../../core/services/article.service';
import { AdminPartComponent } from './admin/admin-part/admin-part.component';
import { SingleUserAdminComponent } from './admin/single-user-admin/single-user-admin.component';


@NgModule({
  declarations: [
    UsersComponent,
    RegisterComponent,
    LoginComponent,
    UserProfileComponent,
    SingleArticleUserComponent,
    AdminPartComponent,
    SingleUserAdminComponent],
  imports: [
    CommonModule, UsersRoutingModule, SharedModule,
  ],
  providers: [ArticleService]

})
export class UsersModule { }
