import { Routes, Router } from '@angular/router';
import { HomeComponent } from '../../home/home.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { AuthService } from '../../../core/services/auth.service';
import { StorageService } from '../../../core/services/storage.service';
import { FormBuilder } from '@angular/forms';
import { NotificationService } from '../../../core/services/notification.service';
import { of, throwError } from 'rxjs';
import { SharedModule } from '../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SingleComponent } from '../../single/single.component';
import { RatingComponent } from '../../rating/rating.component';



describe('LoginComponent', () => {

    const routes: Routes = [
        { path: '', redirectTo: 'home', pathMatch: 'full' },
        { path: 'home', component: HomeComponent },
    ];

    const authService = {
        login() { },
        register() { },
    };
    const notificator = {
        success() { },
        error() { },
    };

    let router: Router;
    let fixture: ComponentFixture<LoginComponent>;
    let component: LoginComponent;

    beforeEach(async(() => {
        // clear all spies and mocks
        jest.clearAllMocks();

        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes),
                SharedModule,
                BrowserAnimationsModule,
            ],
            declarations: [HomeComponent, LoginComponent, SingleComponent, RatingComponent],
            providers: [
                AuthService,
                NotificationService,
                StorageService,
                FormBuilder,
            ]
        })
            .overrideProvider(AuthService, { useValue: authService })
            .overrideProvider(NotificationService, { useValue: notificator })
            .compileComponents();

        router = TestBed.get(Router);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();

    });
    it('successful login should notify with notificator', () => {

        const user = { username: 'bobi', password: '123456' };
        const successSpy = jest.spyOn(notificator, 'success');
        const loginSpy = jest.spyOn(authService, 'login').mockImplementation(() => of(true));
        component.login(user);
        fixture.detectChanges();
        expect(notificator.success).toHaveBeenCalledWith(`Login successful!`);
    });

    it('unsuccessful login should notify with notificator', () => {

        const user = { username: '', password: '' };

        const errorSpy = jest.spyOn(notificator, 'error');
        const loginSpy = jest.spyOn(authService, 'login').mockImplementation(() => throwError(new Error()));

        component.login(user);

        fixture.detectChanges();

        expect(notificator.error).toHaveBeenCalledWith(`Invalid email/password!`);

    });
    it('successful login should redirect to /home', () => {

        const user = { username: 'admin', password: 'testpassword' };

        const navigateSpy = jest.spyOn(router, 'navigate');
        const loginSpy = jest.spyOn(authService, 'login').mockImplementation(() => of(true));

        component.login(user);

        fixture.detectChanges();

        expect(router.navigate).toHaveBeenCalledWith(['home']);

    });



});
