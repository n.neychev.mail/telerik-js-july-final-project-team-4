
import { NotificationService } from '../../../core/services/notification.service';
import { AuthService } from '../../../core/services/auth.service';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserCredentialsDTO } from '../../../common/dto/user/user-credentials.dto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  public login(user: UserCredentialsDTO) {
    this.authService.login(user)
    .subscribe(
      () => {
        this.notificator.success(`Login successful!`);
        this.router.navigate(['home']);
      },
      (error) => {
        this.notificator.error('Invalid email/password!');
      },
    );
  }

}
