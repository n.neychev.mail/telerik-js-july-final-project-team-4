import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ArticleService } from '../../../core/services/article.service';
import { Router } from '@angular/router';
import { PermissionService } from '../../../core/services/permission.service';

@Component({
  selector: 'app-single-article-user',
  templateUrl: './single-article-user.component.html',
  styleUrls: ['./single-article-user.component.css']
})
export class SingleArticleUserComponent implements OnInit, OnDestroy {
private articles;
public permission: boolean;
  private showUpdate: boolean;
  constructor(
    private readonly articleService: ArticleService,
    private readonly permissionService: PermissionService,
    private readonly router: Router,
  ) { }
  @Input() article;

  @Output() deleteArticleClick: EventEmitter<any> = new EventEmitter<any>();
  ngOnInit(

  ) {
  }
  public goToCreateArticle(): void {
    this.router.navigate([`articles/create`]);
  }
  public deleteArticle(): void {

    const id: string = (this.article.id);
    this.articleService.deleteArticle(id).subscribe(data => {
      return this.article = data;
    });
    this.deleteArticleClick.emit(this.article);
  }
  public onArticleButtonClick(): void {
    this.router.navigate([`articles/${this.article.id}`, ]);
  }
  public updateArticle(): void {
    this.articleService.articleToUpdate = this.article;
    this.router.navigate([`articles/${this.article.id}`, ]);
  }
  ngOnDestroy() {
  }

}
