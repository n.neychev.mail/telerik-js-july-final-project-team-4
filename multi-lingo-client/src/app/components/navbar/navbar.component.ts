import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { RegisteredUserDTO } from '../../common/dto/user/register-user.dto';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  public loggedUserData;
  private loggedUserSubscription: Subscription;

  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: RegisteredUserDTO) => (this.loggedUserData = data)
    );
  }

  public ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
  }
}
