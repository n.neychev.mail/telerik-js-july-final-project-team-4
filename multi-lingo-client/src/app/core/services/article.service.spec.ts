import { ArticleService } from './article.service';
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { LanguageService } from './language.service';

describe('Article service', () => {

  const http = {
    get() { },
    post() { },
    put() { },
    patch() { },
    delete() { },
  };
  const languageService = {
    userLanguage: 'en',
  };

  let getService: () => ArticleService;
  beforeEach(() => {
    jest.clearAllMocks();
    TestBed.configureTestingModule({
      imports: [],
      declarations: [],
      providers: [HttpClient, ArticleService, LanguageService]
    })
      .overrideProvider(HttpClient, { useValue: http })
      .overrideProvider(LanguageService, { useValue: languageService });
    getService = () => TestBed.get(ArticleService);
  });

  it(' should get all articles from server', (done) => {
    // Arrange
    const service = getService();
    const spy = jest.spyOn(http, 'get').mockImplementation();
    // Act
    service.getAllArticles();
    // Assert
    expect(http.get).toBeCalledTimes(1);
    done();

  });

  it(' should get article by id from server', (done) => {
    // Arrange
    const service = getService();
    const spy = jest.spyOn(http, 'get').mockImplementation();
    // Act
    service.getArticleById('articleId');
    // Assert
    expect(http.get).toBeCalledTimes(1);
    done();
  });

  it(' should change current article', (done) => {
    // Arrange
    const service = getService();
    const spy = jest.spyOn(http, 'patch').mockImplementation();
    // Act
    service.changeCurrentArticle('articleId', {});
    // Assert
    expect(http.patch).toBeCalledTimes(1);
    done();
  });

  it(' should delete article', (done) => {
    // Arrange
    const service = getService();
    const spy = jest.spyOn(http, 'delete').mockImplementation();
    // Act
    service.deleteArticle('articleId');
    // Assert
    expect(http.delete).toBeCalledTimes(1);
    done();
  });

  it(' should crete article', (done) => {
    // Arrange
    const service = getService();
    const spy = jest.spyOn(http, 'post').mockImplementation();
    // Act
    service.createArticle({
      text: 'text',
      title: 'title',
    });
    // Assert
    expect(http.post).toBeCalledTimes(1);
    done();
  });

  it(' should update article version', (done) => {
    // Arrange
    const service = getService();
    const spy = jest.spyOn(http, 'put').mockImplementation();
    // Act
    service.createNewVersion('articleId', {
      text: 'text',
      title: 'title',
    });
    // Assert
    expect(http.put).toBeCalledTimes(1);
    done();
  });
});
