import { Injectable } from '@angular/core';
import { UiComponentDTO } from '../../common/dto/ui-component/ui-component.dto';
import { CONFIG } from '../../config/config';
import { LanguageService } from './language.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CreateComponentDTO } from '../../common/dto/ui-component/create-component.dto';

@Injectable()
export class UiComponentService {

  public constructor(
    private readonly http: HttpClient,
    private readonly languageService: LanguageService,
  ) { }

  public getUiComponents(componentList: Array<string>): Observable<UiComponentDTO[]> {
    return this.http.get<UiComponentDTO[]>(
      `${CONFIG.DOMAIN_NAME}/ui-component?language=${this.languageService.userLanguage}&componentStrings=${componentList}`
    );
  }

  public createUiComponent(body: CreateComponentDTO): Observable<UiComponentDTO> {
    return this.http.post<UiComponentDTO>(`${CONFIG.DOMAIN_NAME}/ui-component`, body);
  }

  public updateUiComponent(componentId: string , body: CreateComponentDTO): Observable<UiComponentDTO> {
    return this.http.put<UiComponentDTO>(`${CONFIG.DOMAIN_NAME}/ui-component/${componentId}`, body);
  }
}
