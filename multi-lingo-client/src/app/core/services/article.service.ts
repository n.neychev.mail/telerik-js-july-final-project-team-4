import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { CONFIG } from '../../config/config';
import { ArticleDTO } from '../../common/dto/article/article.dto';
import { SingleArticleDTO } from '../../common/dto/article/single-article.dto';
import { LanguageService } from './language.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  public articleToUpdate;
  public constructor(
    private readonly http: HttpClient,
    private readonly languageService: LanguageService,
    ) { }

  public getAllArticles(): Observable<ArticleDTO[]> {
    return this.http.get<ArticleDTO[]>(`${CONFIG.DOMAIN_NAME}/articles?language=${this.languageService.userLanguage}`);
  }
  public getArticleById(id: string): Observable<SingleArticleDTO> {
    return this.http.get<SingleArticleDTO>(`${CONFIG.DOMAIN_NAME}/articles/${id}`);
  }
  public changeCurrentArticle(versionId: string, body): Observable<any> {
    return this.http.patch<any>(`${CONFIG.DOMAIN_NAME}/articles/version/${versionId}`, body);
  }
  public deleteArticle(id: string): Observable<any> {
    return this.http.delete<any>(`${CONFIG.DOMAIN_NAME}/articles/${id}`);
  }
  public createArticle(body): Observable<any> {
    return this.http.post<any>(`${CONFIG.DOMAIN_NAME}/articles/`, body);
  }
  public createNewVersion(articleId: string, body): Observable<any> {
    return this.http.put<any>(`${CONFIG.DOMAIN_NAME}/articles/${articleId}`, body);
  }
}
