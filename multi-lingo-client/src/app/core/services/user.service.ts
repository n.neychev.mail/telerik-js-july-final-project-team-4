import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';

import { UserDTO } from '../../common/dto/user/user.dto';
import { ShowUserDTO } from '../../common/dto/user/show-user.dto';

import { ReturnUserAdminPanelDTO } from '../../common/dto/user/return-user-admin-panel.dto';
import { AdminActionDTO } from '../../common/dto/admin/admin-action.dto';
import { LanguageDTO } from '../../common/dto/language/language.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public constructor(private readonly http: HttpClient) { }

  public getAllUsers(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(`${CONFIG.DOMAIN_NAME}/users`);
  }
  public getUsersById(userId: string): Observable<ShowUserDTO> {
    return this.http.get<ShowUserDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}`);
  }
  public updateUserLanguage(userId: string, body: LanguageDTO): Observable<ShowUserDTO> {
    return this.http.patch<ShowUserDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/language`, body);
  }
  public deleteUser(userId: string): Observable<ReturnUserAdminPanelDTO> {
    return this.http.delete<ReturnUserAdminPanelDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}`);
  }
  public changeUserRole(userId: string, action: AdminActionDTO): Observable<ReturnUserAdminPanelDTO> {
    return this.http.patch<ReturnUserAdminPanelDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/group`, action);
  }
}
