import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CONFIG } from '../../config/config';
import { StorageService } from './storage.service';
import { RegisteredUserDTO } from '../../common/dto/user/register-user.dto';
import { UserCredentialsDTO } from '../../common/dto/user/user-credentials.dto';

@Injectable()
export class AuthService {

  private readonly helper = new JwtHelperService();

  private loggedUserDataSubject$ = new BehaviorSubject<RegisteredUserDTO>(
    this.getUserDataIfAuthenticated()
  );

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) {
  }

  public get loggedUserData$(): Observable<RegisteredUserDTO> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public emitUserData(user: RegisteredUserDTO): void {
    this.loggedUserDataSubject$.next(user);
  }

  public getUserDataIfAuthenticated(): RegisteredUserDTO {
    const token: string = this.storage.getItem('token');

    if (token && this.helper.isTokenExpired(token)) {
      this.storage.removeItem('token');
      return null;
    }

    return token ? this.helper.decodeToken(token) : null;
  }

  public register(user: RegisteredUserDTO) {
    user.languagePreference = navigator.language;
    return this.http.post(`${CONFIG.DOMAIN_NAME}/users`, user);
  }

  public login(user: UserCredentialsDTO) {
    return this.http.post<{ token: string }>(`${CONFIG.DOMAIN_NAME}/session`, user)
      .pipe(
        tap(({ token }) => {
          try {
            this.storage.setItem('token', token);
            const userData: RegisteredUserDTO = this.helper.decodeToken(token);
            this.emitUserData(userData);
          } catch (error) {
          }
        }),
      );
  }
  public logout() {
    this.storage.clear();
    this.emitUserData(null);
  }

}
