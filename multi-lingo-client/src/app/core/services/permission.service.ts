import { Injectable } from '@angular/core';

import { RegisteredUserDTO } from '../../common/dto/user/register-user.dto';


import { AuthService } from './auth.service';



import { CONFIG } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { PermissionName } from '../../common/enum/permission.enum';

@Injectable()
export class PermissionService {
  private userPermissions: Set<string> = new Set<string>();

  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
  ) {
    this.authService.loggedUserData$.subscribe(
      async (data: RegisteredUserDTO) => {
        if (data) {
          const permissions = await this.getUserPermissions(data.id);
          this.userPermissions =  new Set<string>(permissions);
        }

      }
    );
  }

  public getUserPermissions(userId: string): Promise<Set<string>> {
    return this.http.get<Set<string>>(`${CONFIG.DOMAIN_NAME}/users/${userId}/permissions`).toPromise();
  }

  public isUserPermitted(permission: PermissionName): boolean {
    return this.userPermissions.has(permission);

  }

}
