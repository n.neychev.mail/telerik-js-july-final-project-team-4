import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { LanguageDTO } from '../../common/dto/language/language.dto';
import { CONFIG } from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { RegisteredUserDTO } from '../../common/dto/user/register-user.dto';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { StorageService } from './storage.service';
import { ShowUserDTO } from '../../common/dto/user/show-user.dto';


@Injectable()
export class LanguageService {
  private userLanguagePreference: LanguageDTO;
  private languageDataSubject$ = new ReplaySubject<LanguageDTO[]>(1);
  private allLanguages: LanguageDTO[];

  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly storageService: StorageService,
  ) {
    this.getAllLanguages().then(
      this.emitLanguageData.bind(this)
    );

    this.authService.loggedUserData$.subscribe(
      async (data: RegisteredUserDTO) => {
        if(data){
          const user = await this.userService.getUsersById(data.id).toPromise();
          this.updateUserPreferenceLanguage(user.languagePreference);
        }
      });
  }

  public get languageData$(): Observable<LanguageDTO[]> {
    return this.languageDataSubject$.asObservable();
  }
  private getLocalLanguage(): string {
    const localLanguage = navigator.languages
      ? navigator.languages[0]
      : (navigator.language || navigator['userLanguage']);
    return localLanguage.substring(0, 2);
  }

  public emitLanguageData(languages: LanguageDTO[]): void {
    this.allLanguages = languages;
    this.languageDataSubject$.next(languages);
  }

  public get userLanguage(): string {
    return this.languagePreference ? this.languagePreference.symbol : this.getLocalLanguage();
  }

  public get languagePreference(): LanguageDTO {
    if (this.userLanguagePreference) {
      return this.userLanguagePreference;
    } else if (this.storageService.getItem('language')) {
      return JSON.parse(this.storageService.getItem('language'));
    }
    return null;
  }
  public updateUserPreferenceLanguage(languagePreference: LanguageDTO): void {
    this.userLanguagePreference = languagePreference;
    if (languagePreference) {
      this.storageService.setItem('language', JSON.stringify(languagePreference));
    }
  }

  public updateUserLanguageRequest(userId: string, languagePreference: LanguageDTO): LanguageDTO {
    let returnLanguage: LanguageDTO;
    this.userService.updateUserLanguage(userId, languagePreference).subscribe((userData: ShowUserDTO) => {
      this.updateUserPreferenceLanguage(userData.languagePreference);
      returnLanguage = userData.languagePreference;
    });
    return returnLanguage;
  }

  public getAllLanguages(): Promise<LanguageDTO[]> {
    return this.http.get<LanguageDTO[]>(`${CONFIG.DOMAIN_NAME}/languages`).toPromise();
  }
}
