import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';
import { SingleArticleDTO } from '../../common/dto/article/single-article.dto';
import { TranslationDTO } from '../../common/dto/translation/translation.dto';
import { UpdateTranslationDTO } from '../../common/dto/translation/update-translation.dto';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  public translation: TranslationDTO;

  public constructor(
    private readonly http: HttpClient
  ) { }

  public getAllTranslations(): Observable<TranslationDTO[]> {
    return this.http.get<TranslationDTO[]>(`${CONFIG.DOMAIN_NAME}/translations`);
  }
  public getArticleById(id: string): Observable<SingleArticleDTO> {
    return this.http.get<SingleArticleDTO>(`${CONFIG.DOMAIN_NAME}/articles/${id}`);
  }
  public updateTranslation(translationId: string, body: UpdateTranslationDTO): Observable<TranslationDTO> {
    return this.http.put<TranslationDTO>(`${CONFIG.DOMAIN_NAME}/translations/${translationId}`, body);


  }
  public rateTranslations(body): Observable<TranslationDTO> {
    return this.http.patch<TranslationDTO>(`${CONFIG.DOMAIN_NAME}/translations/rate`, body);
  }
}
