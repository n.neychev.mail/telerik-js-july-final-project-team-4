import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  constructor() { }

  public getItem(key: string): string {
    const value =  localStorage.getItem(key);
    return value && value !== 'undefined'
      ? value
      : null;
  }

  public setItem(key: string, value: string): void {
    localStorage.setItem(key, String(value));
  }

  public removeItem(key: string): void {
    localStorage.removeItem(key);
  }

  public clear(): void {
    localStorage.clear();
  }
}
