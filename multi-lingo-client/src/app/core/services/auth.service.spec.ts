import { AuthService } from './auth.service';
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { combineLatest, of } from 'rxjs';
import { UserCredentialsDTO } from '../../common/dto/user/user-credentials.dto';
import { RegisteredUserDTO } from '../../common/dto/user/register-user.dto';

describe('Auth service', () => {
    const http = {
        post() { },
    };
    const storage = {
        getItem() { return ''; },
        setItem() { },
        clear() { },
        removeItem() {}
    };
    let getService: () => AuthService;
    beforeEach(() => {
        jest.clearAllMocks();
        TestBed.configureTestingModule({
            imports: [],
            declarations: [],
            providers: [StorageService, HttpClient, AuthService]
        })
            .overrideProvider(HttpClient, { useValue: http })
            .overrideProvider(StorageService, { useValue: storage });
        getService = () => TestBed.get(AuthService);
    });
    it(' should  handle  invalid  tokens', (done) => {
        const token = 'token';
        const spy = jest.spyOn(storage, 'setItem').mockImplementation(() => token);
        const service = getService();
        combineLatest(service.loggedUserData$).subscribe(
            ([loggedUserData]) => {
                expect(loggedUserData).toBe(null);
                done();
            }
        );
    });
    it(' should correctly initialize state with a valid token', (done) => {
        // tslint:disable-next-line: max-line-length
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImM5NGI1OTdjLWExYzYtNDA4My04OTk2LWZiMmEzNGM5MzI0ZSIsInVzZXJuYW1lIjoiQWRtaW4iLCJpYXQiOjE1NzYyNDAxMDQsImV4cCI6MTU3NjI2NDEwNH0.mys96leIBtrjL28a7fhHd-UTIeB84FM09KSt6CFPkqA';
        const spy = jest.spyOn(storage, 'getItem').mockImplementation(() => token);
        const service = getService();
        combineLatest(service.loggedUserData$).subscribe(
            ([loggedUserData]) => {
                expect(loggedUserData).toBeDefined();
                done();
            }
        );
    });
    it(' login() should call http.post with the correct parameters', (done) => {
        // tslint:disable-next-line: max-line-length
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImM5NGI1OTdjLWExYzYtNDA4My04OTk2LWZiMmEzNGM5MzI0ZSIsInVzZXJuYW1lIjoiQWRtaW4iLCJpYXQiOjE1NzYyNDAxMDQsImV4cCI6MTU3NjI2NDEwNH0.mys96leIBtrjL28a7fhHd-UTIeB84FM09KSt6CFPkqA';
        const postSpy = jest.spyOn(http, 'post').mockImplementation(() => of({ token }));
        const saveSpy = jest.spyOn(storage, 'setItem');
        const service = getService();
        const user = new UserCredentialsDTO();


        service.login(user).subscribe(
            () => {
                expect(storage.setItem).toHaveBeenCalledTimes(1);
                expect(storage.setItem).toHaveBeenCalledWith('token', token);

                done();

            }
        );
    });
    it('login() should call storage.save with the return valid token', (done) => {

        // tslint:disable-next-line: max-line-length
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

        const postSpy = jest.spyOn(http, 'post').mockImplementation(() => of({ token }));
        const saveSpy = jest.spyOn(storage, 'setItem');

        const service = getService();
        const user = new UserCredentialsDTO();

        service.login(user).subscribe(
            () => {
                expect(storage.setItem).toHaveBeenCalledTimes(1);
                expect(storage.setItem).toHaveBeenCalledWith('token', token);

                done();
            }
        );

    });
    it('login() should call http.post with the correct parameters', () => {

        // tslint:disable-next-line: max-line-length
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

        const spy = jest.spyOn(http, 'post').mockImplementation(() => of({ token }));

        const service = getService();
        const user = new UserCredentialsDTO();

        service.login(user);

        expect(http.post).toHaveBeenCalledTimes(1);
        expect(http.post).toHaveBeenLastCalledWith(`http://localhost:3000/session`, user);
    });
    it('login() should call storage.save with the return valid token', (done) => {

        // tslint:disable-next-line: max-line-length
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

        const postSpy = jest.spyOn(http, 'post').mockImplementation(() => of({ token }));
        const saveSpy = jest.spyOn(storage, 'setItem');

        const service = getService();
        const user = new UserCredentialsDTO();

        service.login(user).subscribe(
            () => {
                expect(storage.setItem).toHaveBeenCalledTimes(1);
                expect(storage.setItem).toHaveBeenCalledWith('token', token);

                done();
            }
        );

    });

    it('logout() should call storage.save', () => {
        const token  = '';
        const spy = jest.spyOn(storage, 'clear').mockImplementation(() => {});
        const service = getService();
        service.logout();
        expect(storage.clear).toBeCalledTimes(1);
    });
    it('logout() should set the correct state', (done) => {

        // tslint:disable-next-line: max-line-length
        const token  = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';
        const saveSpy = jest.spyOn(storage, 'setItem').mockImplementation(() => {});
        const readSpy = jest.spyOn(storage, 'getItem').mockImplementation(() => token);
        const service = getService();
        service.logout();
        combineLatest(service.loggedUserData$).subscribe(
        ([loggedUser]) => {
            expect(loggedUser).toBe(null);
            done();
            },
        );
    });
    it('register() should call http.post', () => {

        const user = new RegisteredUserDTO();
        const spy = jest.spyOn(http, 'post');

        const service = getService();

        service.register(user);

        expect(http.post).toHaveBeenCalledWith(`http://localhost:3000/users`, user);

    });

});
