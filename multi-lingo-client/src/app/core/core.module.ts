import { NotificationService } from './services/notification.service';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { TranslationService } from './services/translation.service';
import { LanguageService } from './services/language.service';
import { PermissionService } from './services/permission.service';

@NgModule({
  providers: [AuthService, NotificationService, StorageService, TranslationService, LanguageService, PermissionService],
  declarations: [],
  imports: [
    CommonModule, HttpClientModule
  ],
  exports: [HttpClientModule]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error(`CoreModule has already been initialized!`);
    }
  }
}
