import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

import { Observable } from 'rxjs';
import { StorageService } from '../../core/services/storage.service';


@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  public constructor(
    private readonly storageService: StorageService,
    ) {}

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

        const token = this.storageService.getItem('token') || '';
        const updatedRequest = request.clone({
          headers: request.headers.set('Authorization', `Bearer ${token}`)
        });

        return next.handle(updatedRequest);
      }
    }
