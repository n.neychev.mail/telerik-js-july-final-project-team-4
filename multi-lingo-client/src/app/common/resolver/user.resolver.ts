import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../../core/services/user.service';
import { ShowUserDTO } from '../dto/user/show-user.dto';

@Injectable()
export class UserResolver implements Resolve<ShowUserDTO> {
  constructor(private readonly userService: UserService ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<ShowUserDTO>|Promise<ShowUserDTO>|ShowUserDTO {
    return this.userService.getUsersById(route.paramMap.get('id')).toPromise();
  }
}
