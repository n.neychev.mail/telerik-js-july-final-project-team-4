export enum PermissionName {
  Read_Article = 'Read Article',
  Update_Article = 'Update Article',
  Delete_Article = 'Delete Article',
  Create_Article = 'Create Article',
  Update_Translation = 'Update Translation',
  Read_Translation = 'Read Translation',
  Create_User = 'Create User',
  Read_User = 'Read User',
  Update_User = 'Update User',
  Delete_User = 'Delete User',
}
