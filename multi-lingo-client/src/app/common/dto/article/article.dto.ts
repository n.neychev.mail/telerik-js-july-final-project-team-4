

export class ArticleDTO {
    public id: string;
    public title: string;
    public text: string;
}
