
import { SingleVersionDTO } from '../../version/dto/single-version.dto';

export class SingleArticleDTO {
    public id: string;
    public isDeleted: boolean;
    public versions: SingleVersionDTO[];
}
