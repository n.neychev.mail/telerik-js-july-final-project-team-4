export class UpdateUserDTO {
  public username: string;
  public password: string;
  public languagePreference: string;
}
