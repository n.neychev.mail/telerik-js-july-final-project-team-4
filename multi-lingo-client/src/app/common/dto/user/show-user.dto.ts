import { LanguageDTO } from '../language/language.dto';
import { ArticleDTO } from '../article/article.dto';

export class ShowUserDTO {
  public id: string;
  public username: string;
  public isDeleted: boolean;
  public languagePreference: LanguageDTO;
  public article: ArticleDTO;
}
