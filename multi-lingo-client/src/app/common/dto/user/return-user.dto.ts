import { SingleVersionDTO } from '../../version/dto/single-version.dto';
import { LanguageDTO } from '../language/language.dto';

export class ReturnUserDTO {
    public id: string;
    public username: string;
    public article: SingleVersionDTO;
    public isDeleted: boolean;
  public languagePreference: LanguageDTO;
}
