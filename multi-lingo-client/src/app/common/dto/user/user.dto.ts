import { LanguageDTO } from '../language/language.dto';
import { ArticleDTO } from '../article/article.dto';
import { GroupDTO } from '../group/group.dto';

export class UserDTO {
  public id: string;
  public username: string;
  public isDeleted: boolean;
  public languagePreference: LanguageDTO;
  public articles: ArticleDTO;
  public group: GroupDTO[];
}
