import { GroupDTO } from '../group/group.dto';

export class ShowUserAdminPanelDTO {
  public id: string;
  public username: string;
  public group: GroupDTO[];
}
