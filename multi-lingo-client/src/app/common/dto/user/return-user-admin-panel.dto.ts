import { GroupDTO } from '../group/group.dto';

export class ReturnUserAdminPanelDTO {
    public id: string;
    public username: string;

    public group: GroupDTO[];
}
