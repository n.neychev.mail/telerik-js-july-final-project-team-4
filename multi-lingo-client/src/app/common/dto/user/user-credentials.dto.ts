export class UserCredentialsDTO {
  public username: string;
  public password: string;
}
