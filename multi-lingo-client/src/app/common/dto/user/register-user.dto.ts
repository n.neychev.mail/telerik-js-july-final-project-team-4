export class RegisteredUserDTO {
      public id: string;
      public username: string;
      public languagePreference: string;
}
