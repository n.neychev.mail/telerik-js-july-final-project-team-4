import { UiTranslationDTO } from './ui-translation.dto';

export class UiComponentDTO {
    public id: string;
    public text: string;
    public translations: UiTranslationDTO;
}
