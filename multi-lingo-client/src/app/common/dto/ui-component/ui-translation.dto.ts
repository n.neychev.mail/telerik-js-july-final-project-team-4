import { LanguageDTO } from '../language/language.dto';

export class UiTranslationDTO {
    public id: string;
    public text: string;
    public language: LanguageDTO;

}
