export class TranslationDTO {
  public id: string;
  public text: string;
}
