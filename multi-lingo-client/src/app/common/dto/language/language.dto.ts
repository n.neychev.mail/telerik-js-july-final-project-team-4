export class LanguageDTO {
  public name: string;
  public symbol: string;
}
