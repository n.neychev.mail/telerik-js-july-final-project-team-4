export class SingleVersionDTO {
    public id: string;
    public number?: number;
    public text: string;
    public title: string;
}
