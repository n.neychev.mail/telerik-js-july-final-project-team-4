import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificationService } from '../../core/services/notification.service';
import { PermissionService } from '../../core/services/permission.service';
import { PermissionName } from '../enum/permission.enum';
import { AuthService } from '../../core/services/auth.service';
import { RegisteredUserDTO } from '../dto/user/register-user.dto';

@Injectable({
  providedIn: 'root'
})

export class PermissionGuard implements CanActivate {

  public loggedUserData: RegisteredUserDTO;

  constructor(
    private readonly permissionService: PermissionService,
    private readonly notificationService: NotificationService,
    private readonly authService: AuthService,
  ) {
    this.authService.loggedUserData$.subscribe(
      (data: RegisteredUserDTO) => this.loggedUserData = data);
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const permissionToCheck: PermissionName = route.data.permission;

    if (route.data.id === this.loggedUserData.id) {
      return true;
    }
    if (!this.permissionService.isUserPermitted(permissionToCheck)) {
      this.notificationService.error(
        `You are not permitted to see this page!`
      );
      return false;
    }
    return true;
  }
}
