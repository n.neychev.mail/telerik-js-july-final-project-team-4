import { ValidatorFn, AbstractControl } from '@angular/forms';

export function changedInput(objectValue: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const propertyChanged = (objectValue === control.value);
    return propertyChanged ? { changedValue: { value: control.value } } : null;
  };
}

